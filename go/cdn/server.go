package cdn

import (
	"net/http"
)

func NewServer() http.Handler {
	mux := http.NewServeMux()
	mux.Handle("/", http.FileServer(http.Dir("cdnAssets")))
	return mux
}
