package context

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/jinzhu/gorm"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type Configuration struct {
	DbUrl          string
	DbType         string
	GormDb         *gorm.DB
	Logger         *log.Logger
	MaxOrders      uint
	AwsSecretKey   string
	AwsAccessKey   string
	Bucket         string
	ObjectStoreUrl string
	Templates      *template.Template
	AdminAssets    []map[string]string
	AppAssets      []map[string]string
}

var Config *Configuration

func parseTemplates() {
	var allFiles []string
	files, err := ioutil.ReadDir("./templates")
	if err != nil {
		fmt.Println(err)
	}
	for _, file := range files {
		filename := file.Name()
		if strings.HasSuffix(filename, ".tmpl") {
			allFiles = append(allFiles, "./templates/"+filename)
		}
	}

	templates := template.Must(template.ParseFiles(allFiles...))
	Config.Templates = templates
}

func configureAssetsFor(p string, v interface{}) {
	f, err := os.Open(p)
	if err != nil {
		Config.Logger.Fatalln("Forgot to build app?")
	}

	defer f.Close()
	decoder := json.NewDecoder(f)
	err = decoder.Decode(v)
	if err != nil {
		Config.Logger.Fatalln("Unable to parse config/app.json: ", err.Error())
	}
}

func configureAssets() {
	configureAssetsFor("config/app.json", &Config.AppAssets)
	configureAssetsFor("config/adminAssets.json", &Config.AdminAssets)
}

func init() {
	Config = new(Configuration)
	Config.Logger = log.New(os.Stdout, "LOG: ", log.Lshortfile)
	f, err := os.Open("config/config.json")
	if err != nil {
		Config.Logger.Println("Unable to found config/config.json. Falling back to defaults")
		Config.DbUrl = "postgres://quadb:quadb@db/quadb?sslmode=disable"
		Config.DbType = "postgres"
	} else {
		defer f.Close()
		decoder := json.NewDecoder(f)
		err := decoder.Decode(&Config)
		if err != nil {
			Config.Logger.Fatal("Invalid configuration " + err.Error())
		}
	}

	if err != nil {
		Config.Logger.Fatal("Unable to connect to database " + err.Error())
	}

	Config.GormDb, err = gorm.Open(Config.DbType, Config.DbUrl)
	if err != nil {
		Config.Logger.Fatal("Unable to connect to database " + err.Error())
	}

	Config.GormDb.LogMode(true)

	Config.GormDb.Exec("create extension hstore;")
	Config.Logger.Println("Successfully created connection to " + Config.DbType + " server")

	configureAssets()
}
