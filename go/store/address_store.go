package store

import (
	"bitbucket.org/dhaliwalprince/quadb/go/models"
	"github.com/jinzhu/gorm"
)

type AddressStore struct {
	DB *gorm.DB
}

func (store *AddressStore) GetAddressesBy(user *models.User) ([]models.Address, error) {
	addresses := []models.Address{}
	err := store.DB.Model(user).Related(&addresses).Error
	return addresses, err
}

func (store *AddressStore) Save(user *models.User, address models.Address) error {
	return store.DB.Model(user).Save(&address).Error
}

func (store *AddressStore) Update(user *models.User, address models.Address) error {
	return store.DB.Model(user).Updates(address).Error
}

func (store *AddressStore) Delete(user *models.User, address models.Address) error {
	return store.DB.Model(user).Delete(address).Error
}

func (store *AddressStore) FindBy(user *models.User, id int) (*models.Address, error) {
	address := models.Address{}
	err := store.DB.Model(user).Where(id).First(&address).Error
	return &address, err
}
