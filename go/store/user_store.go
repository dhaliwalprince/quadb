package store

import (
	"bitbucket.org/dhaliwalprince/quadb/go/models"
	"github.com/jinzhu/gorm"
)

type UserStore struct {
	DB *gorm.DB
}

func (store *UserStore) GetAll() ([]models.User, error) {
	users := []models.User{}
	err := store.DB.Find(&users).Error
	return users, err
}

func (store *UserStore) GetUserById(id int) (*models.User, error) {
	user := models.User{}
	err := store.DB.Where(id).First(&user).Error
	if err != nil {
		return nil, err
	}

	err = store.DB.Model(user).Related(&user.Addresses).Error
	return &user, err
}

func (store *UserStore) GetUserByEmail(email string) (*models.User, error) {
	user := models.User{}
	err := store.DB.Where("email = ?", email).First(&user).Error
	return &user, err
}

func (store *UserStore) Create(user models.User) error {
	return store.DB.Save(&user).Error
}

func (store *UserStore) UpdatePassword(user models.User) error {
	return store.DB.Model(&user).Update("password", user.Password).Error
}
