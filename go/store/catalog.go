package store

import (
	"bitbucket.org/dhaliwalprince/quadb/go/models"
	"github.com/jinzhu/gorm"
)

type Catalog struct {
	DB *gorm.DB
}

func (catalog *Catalog) GetCategories() ([]models.Category, error) {
	categories := []models.Category{}
	err := catalog.DB.Find(&categories).Error
	return categories, err
}

func (catalog *Catalog) GetCategoryById(id int) (*models.Category, error) {
	category := models.Category{}
	err := catalog.DB.Where(id).First(&category).Error
	return &category, err
}

func (catalog *Catalog) AddCategory(category models.Category) error {
	return catalog.DB.Save(&category).Error
}

func (catalog *Catalog) RemoveCategory(category models.Category) error {
	return catalog.DB.Delete(category).Error
}

func (catalog *Catalog) UpdateCategory(category models.Category) error {
	return catalog.DB.Updates(&category).Error
}

func (catalog *Catalog) GetClothMaterials() ([]models.ClothMaterial, error) {
	materials := []models.ClothMaterial{}
	err := catalog.DB.Find(&materials).Error
	return materials, err
}

func (catalog *Catalog) GetClothMaterialById(id int) (*models.ClothMaterial, error) {
	material := models.ClothMaterial{}
	err := catalog.DB.Where(id).First(&material).Error
	return &material, err
}

func (catalog *Catalog) AddClothMaterial(material models.ClothMaterial) error {
	return catalog.DB.Save(&material).Error
}

func (catalog *Catalog) RemoveClothMaterial(material models.ClothMaterial) error {
	return catalog.DB.Delete(material).Error
}

func (catalog *Catalog) UpdateClothMaterial(material models.ClothMaterial) error {
	return catalog.DB.Updates(&material).Error
}

func (catalog *Catalog) GetPrintingTypes() ([]models.PrintCategory, error) {
	categories := []models.PrintCategory{}
	err := catalog.DB.Find(&categories).Error
	return categories, err
}

func (catalog *Catalog) AddPrintingType(category models.PrintCategory) error {
	return catalog.DB.Save(&category).Error
}

func (catalog *Catalog) RemovePrintingType(category models.PrintCategory) error {
	return catalog.DB.Delete(category).Error
}

func (catalog *Catalog) UpdatePrintingType(category models.PrintCategory) error {
	return catalog.DB.Update(category).Error
}
