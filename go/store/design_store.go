package store

import (
	"bitbucket.org/dhaliwalprince/quadb/go/models"
	"github.com/jinzhu/gorm"
)

type DesignStore struct {
	DB *gorm.DB
}

func (store *DesignStore) GetAllDesigns(offset int) ([]models.Design, error) {
	designs := []models.Design{}
	err := store.DB.Offset(offset).Limit(100).Find(&designs).Error
	return designs, err
}

func (store *DesignStore) GetUserDesigns(user *models.User, offset int) ([]models.Design, error) {
	designs := []models.Design{}
	err := store.DB.Offset(offset).Limit(100).Where("user_id = ?", user.ID).Find(&designs).Error
	return designs, err
}

func (store *DesignStore) GetDesignBy(id int) (models.Design, error) {
	design := models.Design{}
	err := store.DB.Find(&design, id).Error
	return design, err
}

func (store *DesignStore) SaveDesign(user *models.User, design models.Design) error {
	design.UserId = user.ID
	return store.DB.Model(user).Save(&design).Error
}

func (store *DesignStore) DeleteDesign(user *models.User, design models.Design) error {
	return store.DB.Model(user).Delete(design).Error
}

func (store *DesignStore) GetDesignByHash(user *models.User, hash string) (design models.Design, err error) {
	err = store.DB.Where("image_url LIKE '%' || ? || '%'", hash).First(&design).Error
	return
}
