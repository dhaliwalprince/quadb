package store

import (
	"errors"

	"bitbucket.org/dhaliwalprince/quadb/go/models"
	"github.com/jinzhu/gorm"
)

type OrderStore struct {
	DB *gorm.DB
}

func (store *OrderStore) GetAllOrders() ([]models.Order, error) {
	orders := []models.Order{}
	err := store.DB.Find(&orders).Error

	if err != nil {
		return nil, err
	}

	for i := range orders {
		err = store.FillDetails(&orders[i])
		if err != nil {
			return nil, err
		}
	}
	return orders, err
}

func (store *OrderStore) FillOrders(orders []models.Order) ([]models.Order, error) {
	err := errors.New("")
	for i := range orders {
		err = store.FillDetails(&orders[i])
		if err != nil {
			break
		}
	}

	return orders, err
}

func (store *OrderStore) GetAcceptedOrders() ([]models.Order, error) {
	orders := []models.Order{}
	err := store.DB.Table("order").Select(`"order".*`).Joins(`join order_event on "order".id = "order_event".order_id`).
		Where(`order_event.type in (?) and not exists(select * from "order_event" where "order_event".type in (?) and "order_event".order_id = "order".id)`, []string{models.ORDER_ACCEPTED}, []string{models.ORDER_CANCELLED, models.ORDER_DELIVERED, models.ORDER_REJECTED}).
		Find(&orders).Error

	if err != nil {
		return nil, err
	}
	return store.FillOrders(orders)
}

func (store *OrderStore) GetNewOrders() ([]models.Order, error) {
	orders := []models.Order{}
	err := store.DB.Table("order").Select(`DISTINCT "order".*`).Joins(`join order_event on "order".id = "order_event".order_id`).
		Where(`order_event.type in (?) and not exists(select * from "order_event" where "order_event".type in (?) and "order_event".order_id = "order".id)`, []string{models.ORDER_PLACED}, []string{models.ORDER_ACCEPTED, models.ORDER_CANCELLED, models.ORDER_DELIVERED, models.ORDER_REJECTED}).
		Find(&orders).Error
	if err != nil {
		return nil, err
	}
	return store.FillOrders(orders)
}

func (store *OrderStore) GetRejectedOrders() ([]models.Order, error) {
	orders := []models.Order{}
	err := store.DB.Table("order").Select(`DISTINCT "order".*`).Joins(`join order_event on "order".id = "order_event".order_id`).
		Where(`order_event.type in (?)`, []string{models.ORDER_REJECTED}).
		Find(&orders).Error
	if err != nil {
		return nil, err
	}
	return store.FillOrders(orders)
}

func (store *OrderStore) GetCancelledOrders() ([]models.Order, error) {
	orders := []models.Order{}
	err := store.DB.Table("order").Select(`DISTINCT "order".*`).Joins(`join order_event on "order".id = "order_event".order_id`).
		Where(`order_event.type in (?)`, []string{models.ORDER_CANCELLED}).
		Find(&orders).Error
	if err != nil {
		return nil, err
	}
	return store.FillOrders(orders)
}

func (store *OrderStore) GetDeliveredOrders() ([]models.Order, error) {
	orders := []models.Order{}
	err := store.DB.Table("order").Select(`DISTINCT "order".*`).Joins(`join order_event on "order".id = "order_event".order_id`).
		Where(`order_event.type in (?)`, []string{models.ORDER_DELIVERED}).
		Find(&orders).Error

	if err != nil {
		return nil, err
	}
	return store.FillOrders(orders)
}

func (store *OrderStore) FillDetails(order *models.Order) error {
	err := store.DB.Model(order).Related(&order.OrderDetails, "OrderDetailsId").Error
	if err != nil {
		return err
	}

	err = store.DB.Model(order).Related(&order.Events).Error
	if err != nil {
		return err
	}

	err = store.DB.Model(&order.OrderDetails).Related(&order.OrderDetails.ClothMaterial).Error
	if err != nil {
		return err
	}

	err = store.DB.Model(&order.OrderDetails).Related(&order.OrderDetails.Category).Error
	if err != nil {
		return err
	}

	store.DB.Where("user_id = ? and id = ?", order.UserId, order.OrderDetails.AddressId).First(&order.OrderDetails.Address)
	err = store.DB.Where("user_id = ? and id = ?", order.UserId, order.OrderDetails.DesignId).First(&order.OrderDetails.Design).Error
	return err
}

func (store *OrderStore) GetOrderBy(user *models.User, id uint) (*models.Order, error) {
	order := models.Order{}
	err := store.DB.Model(user).Where(id).First(&order).Error
	if err != nil {
		return nil, err
	}
	return &order, store.FillDetails(&order)
}

func (store *OrderStore) GetOrdersOfUser(user *models.User) ([]models.Order, error) {
	orders := []models.Order{}
	err := store.DB.Model(user).Related(&orders, "UserId").Error

	if err != nil {
		return nil, err
	}

	for i := range orders {
		err = store.FillDetails(&orders[i])
		if err != nil {
			return nil, err
		}
	}
	return orders, err
}

func (store *OrderStore) SaveOrder(user *models.User, order models.Order) error {
	return store.DB.Model(user).Save(&order).Error
}

func (store *OrderStore) DeleteOrder(user *models.User, order models.Order) error {
	return store.DB.Model(user).Delete(order).Error
}

func (store *OrderStore) UpdateOrderInfo(order *models.Order, info models.OrderDetails) error {
	return store.DB.Model(&order.OrderDetails).Update("quantity", info.Quantity).Error
}

func (store *OrderStore) UpdateOrderAddress(order *models.Order) error {
	return store.DB.Model(&order.OrderDetails).Update("address_id", order.OrderDetails.AddressId).Error
}

func (store *OrderStore) FindOrderBy(id interface{}) (*models.Order, error) {
	order := models.Order{}
	err := store.DB.Where(id).Find(&order).Error
	return &order, err
}

func (store *OrderStore) CreateRequest(user *models.User, request models.OrderRequest) error {
	request.UserId = user.ID
	return store.DB.Save(&request).Error
}

func (store *OrderStore) UpdateRequestQuantity(user *models.User, requestId uint, quantity uint) error {
	return store.DB.Model(&models.OrderRequest{}).Where(requestId).Update("quantity", quantity).Error
}

func (store *OrderStore) UpdateRequestStatus(user *models.User, requestId uint, status string) error {
	return store.DB.Model(&models.OrderRequest{}).Where(requestId).Update("status", status).Error
}

func (store *OrderStore) ListUserRequests(user *models.User) (requests []models.OrderRequest, err error) {
	db := store.DB.Preload("Design").Preload("Address").Preload("SizeList")
	err = db.Where("user_id = ?", user.ID).Find(&requests).Error
	return
}

func (store *OrderStore) ListAllRequests(filter string) (requests []models.OrderRequest, err error) {
	db := store.DB.Preload("Design").Preload("Address").Preload("SizeList")
	err = db.Where("status = ?", filter).Find(&requests).Error
	return
}

func (store *OrderStore) GetQuotations(requestId int) (quotations []models.Quotation, err error) {
	db := store.DB.Preload("Request").Preload("Category").Preload("Request.Design").Preload("Request.Address").Preload("ClothMaterial")
	err = db.Where("request_id = ?", requestId).Find(&quotations).Error
	return
}

func (store *OrderStore) CreateQuotation(quotation models.Quotation) (err error) {
	err = store.DB.Save(&quotation).Error
	return
}

func (store *OrderStore) DeleteQuotation(quotation models.Quotation) (err error) {
	err = store.DB.Delete(&quotation).Error
	return
}

func (store *OrderStore) DeleteQuotations(request models.OrderRequest) (err error) {
	err = store.DB.Exec("delete from order_request where request_id = ?", request.ID).Error
	return
}
