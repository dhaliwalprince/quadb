package models

import "time"

type FileType int

const (
	kImage FileType = iota
	kOther
)

type File struct {
	ID        uint      `json:"id"`
	CreatedAt time.Time `json:"created_at"`
	Name      string    `json:"name"`
	OwnedBy   uint      `json:"owned_by"`
	Type      FileType  `json:"type"`
	Url       string    `json:"url"`
	MinioId   string    `json:"minio_id"`
}

func (file *File) TableName() string {
	return "asset"
}
