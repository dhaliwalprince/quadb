package models

type Session struct {
	SessionKey string `db:"key"`
	UserId     string `db:"user_id"`
	IP         string `db:"ip_address"`
}
