package models

const (
	ORDER_IN_CART   = "10:in_cart"
	ORDER_CANCELLED = "200:cancelled"
	ORDER_PLACED    = "30:created"
	ORDER_ACCEPTED  = "40:accepted"
	ORDER_REJECTED  = "500:rejected"
	ORDER_DELIVERED = "100:delivered"
)

// OrderEvent represents the current status of the order
type OrderEvent struct {
	Model
	OrderId   uint   `json:"order_id"`
	Type      string `json:"type"`
	UserId    uint   `json:"user_id"`
	Comments  string `json:"comments"`
	Completed bool   `json:"completed"`
}

// holds the necessary details about the order
type OrderDetails struct {
	Model
	Design          Design        `json:"design" gorm:"ForeignKey:DesignId"`
	DesignId        uint          `json:"design_id"`
	ClothMaterial   ClothMaterial `json:"cloth_material" gorm:"ForeignKey:ClothMaterialId"`
	ClothMaterialId uint          `json:"cloth_material_id"`
	Category        Category      `json:"category" gorm:"ForeignKey:CategoryId"`
	CategoryId      uint          `json:"category_id"`
	ItemPrice       float64       `json:"item_price"`
	Quantity        uint          `json:"quantity"`
	Size            uint          `json:"size"`
	Address         Address       `json:"address" gorm:"ForeignKey:AddressId"`
	AddressId       uint          `json:"address_id"`
}

type OrderRequest struct {
	Model
	Design       Design      `json:"design"`
	DesignId     uint        `json:"design_id"`
	UserId       uint        `json:"user_id"`
	Status       string      `json:"status"`
	UserComments string      `json:"user_comments"`
	Comments     string      `json:"comments"`
	AddressId    uint        `json:"address_id"`
	Address      Address     `json:"address"`
	SizeList     []ShirtSize `json:"size_list" gorm:"ForeignKey:OrderRequestId"`
	NameList     string      `json:"name_list"`
}

type ShirtSize struct {
	Code           string `json:"code"`
	Quantity       uint   `json:"quantity"`
	OrderRequestId uint   `json:"order_request_id"`
}

type Order struct {
	Model
	UserId         uint         `json:"user_id"`
	OrderDetails   OrderDetails `json:"order_details" gorm:"ForeignKey:OrderDetailsId"`
	OrderDetailsId uint         `json:"order_details_id"`
	Events         []OrderEvent `json:"events" gorm:"ForeignKey:OrderId"`
}

type Quotation struct {
	Model
	RequestId       uint          `json:"request_id"`
	Request         OrderRequest  `json:"request"`
	ClothMaterialId uint          `json:"cloth_material_id"`
	ClothMaterial   ClothMaterial `json:"cloth_material" gorm:"ForeignKey:ClothMaterialId"`
	CategoryId      uint          `json:"category_id"`
	Category        Category      `json:"category"`
	PricePerItem    float64       `json:"price_per_item"`
	Quantity        uint          `json:"quantity"`
}
