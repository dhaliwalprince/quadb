package models

type Website struct {
	Title    string          `json:"title"`
	Endpoint string          `json:"endpoint"`
}

type GlobalAttribute struct {
	Model
	Name  string `json:"name"`
	Type  string `json:"type"`
	Value string `json:"value"`
}

type ReactWidget struct {
  Model
  Name         string `json:"name"`
  RepoLocation string `json:"repo_location"`
  InstallRoot  string `json:"install_root"`
  Position     string `json:"position"`
  Version      string `json:"version"`
}
