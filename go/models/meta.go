package models

type Meta struct {
	ID       uint     `json:"id"`
	Name     string   `json:"name"`
	Class    string   `json:"class"`
	Headline string   `json:"headline"`
	Url      string   `json:"url"`
	Urls     []Action `json:"urls" gorm:"ForeignKey:MetaId"`
	Columns  []Column `json:"columns" gorm:"ForeignKey:MetaId"`
}

type Action struct {
	Type     string `json:"type"`
	Endpoint string `json:"endpoint"`
	MetaId   uint   `json:"meta_id"`
}

type Column struct {
	ID            uint   `json:"id"`
	MetaId        uint   `json:"meta_id"`
	Name          string `json:"name"`
	Key           string `json:"key"`
	Editable      bool   `json:"editable"`
	Creatable     bool   `json:"creatable"`
	Required      bool   `json:"required"`
	Type          string `json:"type"`
	IsPrimaryKey  bool   `json:"is_primary_key"`
	IsForeignKey  bool   `json:"is_foreign_key"`
	RelatedMeta   Meta   `json:"related_meta" gorm:"ForeignKey:RelatedMetaId"`
	RelatedKey    string `json:"related_key"`
	RelatedMetaId uint   `json:"related_meta_id"`
}
