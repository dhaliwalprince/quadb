package models

import (
	"time"

	"github.com/jinzhu/gorm"
)

type Model struct {
	ID        uint       `json:"id"`
	CreatedAt time.Time  `json:"created_at"`
	UpdatedAt time.Time  `json:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at"`
}

type UserRole struct {
	UserId uint   `json:"user_id"`
	Role   string `json:"role"`
}

var Models []interface{}

func InitializeModels(db *gorm.DB) {
	db.SingularTable(true)
	Models = []interface{}{&User{}, &UserRole{},
		&Address{},
		&Design{},
		&Category{},
		&ClothMaterial{},
		&OrderEvent{},
		&OrderDetails{},
		&OrderRequest{},
		&PrintCategory{},
		&Quotation{},
		&Order{},
		&ShirtSize{},
		&File{},
	}
	db.AutoMigrate(Models...)
}
