package models

type Address struct {
	Model
	Label   string `json:"label"`
	UserId  uint   `json:"user_id"`
	Line1   string `json:"line1"`
	Line2   string `json:"line2"`
	City    string `json:"city"`
	State   string `json:"state"`
	Country string `json:"country"`
	Pincode string `json:"pincode"`
	Mobile  string `json:"mobile"`
}
