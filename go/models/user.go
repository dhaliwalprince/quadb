package models

import "encoding/json"

type User struct {
	Model
	Name      string    `db:"name" json:"name"`
	Email     string    `db:"email" json:"email"`
	Password  string    `db:"password" json:"-"`
	Addresses []Address `gorm:"ForeignKey:UserId" json:"addresses"`
}

func (user *User) MarshalJSON() ([]byte, error) {
	type Alias User
	return json.Marshal((*Alias)(user))
}
