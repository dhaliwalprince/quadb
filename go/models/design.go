package models

type Category struct {
	ID           uint   `json:"id"`
	Name         string `json:"name"`
	Features     string `json:"features"`
	ThumbnailUrl string `json:"thumbnail_url"`
}

type PrintCategory struct {
	ID           uint   `json:"id"`
	Name         string `json:"name"`
	ThumbnailUrl string `json:"thumbnail_url"`
}

type ClothMaterial struct {
	ID           uint   `json:"id"`
	Name         string `json:"name"`
	Features     string `json:"features"`
	ThumbnailUrl string `json:"thumbnail_url"`
}

type Design struct {
	Model
	Name     string `json:"name"`
	UserId   uint   `json:"user_id"`
	ImageUrl string `json:"image_url"`
	IsEmpty  bool   `json:"is_empty"`
	IsPublic bool   `json:"is_public"`
}
