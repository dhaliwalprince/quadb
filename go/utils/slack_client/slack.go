package slack_client

import "github.com/nlopes/slack"

type SlackMessage struct {
	Channel string `json:"channel"`
	Message string `json:"message"`
}

type Slack struct {
	api *slack.Client
}

func (s *Slack) SendMessage(message *SlackMessage) error {
	messageParams := slack.PostMessageParameters{}
	_, _, err := s.api.PostMessage(message.Channel, message.Message, messageParams)
	if err != nil {
		return err
	}
	return nil
}

func New(key string) (*Slack) {
	return &Slack{ api: slack.New(key)}
}
