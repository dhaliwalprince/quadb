package utils

import (
	"os"
	"github.com/nlopes/slack"
)

var s *slack.Client

func SendSlackMessage(message string) (error) {
	_, _, err := s.PostMessage(os.Getenv("SLACK_CHANNEL"), message, slack.PostMessageParameters{})
	return err
}

func init() {
	s = slack.New(os.Getenv("SLACK_API"))
}
