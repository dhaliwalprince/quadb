package testing

import (
	"github.com/gorilla/mux"
	"net/http"
	"net/http/httptest"
	"testing"
)

func statusHandler(w http.ResponseWriter, request *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("200 OK"))
}

func TestInitRoutes(t *testing.T) {
	router := mux.NewRouter()
	mux := http.NewServeMux()

	router.HandleFunc("/status", statusHandler).Methods("GET")
	recorder := httptest.NewRecorder()
	mux.Handle("/api/", http.StripPrefix("/api", router))

	request := httptest.NewRequest("GET", "/api/status", nil)

	mux.ServeHTTP(recorder, request)

	result := recorder.Body.String()

	if result != "200 OK" {
		t.Errorf("Router failed to route correctly")
	}
}
