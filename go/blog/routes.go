package blog

import (
	"bitbucket.org/dhaliwalprince/quadb/go/context"
	"bitbucket.org/dhaliwalprince/quadb/go/tmpl"
	"github.com/gorilla/mux"
)

func Install(context *tmpl.TemplateContext, config *context.Configuration, router *mux.Router) {
	controller := NewBlogController(config.GormDb)
	blog := context.MakeTemplateHandler(controller.getBlogByIdHandler)
	router.HandleFunc("/blog", blog).Methods("GET")
	router.HandleFunc("/blogs", context.MakeTemplateHandler(controller.getBlogIndexHandler)).Methods("GET")
	router.HandleFunc("/blog/new", context.MakeTemplateHandler(controller.createNewBlogHandler)).Methods("POST")
	router.HandleFunc("/blog/new", context.MakeTemplateHandler(controller.newBlogTemplateHandler)).Methods("GET")
}
