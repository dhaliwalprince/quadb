package blog

import (
	"net/http"
	"time"

	"bitbucket.org/dhaliwalprince/quadb/go/auth"
	"bitbucket.org/dhaliwalprince/quadb/go/tmpl"
	"github.com/jinzhu/gorm"

	// unneccessary dependency on routes, should be removed by DI
	"bitbucket.org/dhaliwalprince/quadb/go/common"
	"bitbucket.org/dhaliwalprince/quadb/go/routes"
	"html/template"
	"log"
)

type Blog struct {
	ID          uint          `json:"id"`
	Author      string        `json:"author"`
	PublishedOn time.Time     `json:"published_on"`
	Title       string        `json:"title"`
	Subtitle    string        `json:"subtitle"`
	Content     template.HTML `json:"content"`
}

type BlogController struct {
	db *gorm.DB
}

func NewBlogController(db *gorm.DB) *BlogController {
	controller := &BlogController{db: db}
	controller.migrate()
	return controller
}

func (controller *BlogController) migrate() {
	controller.db.AutoMigrate(&Blog{})
}

func (controller *BlogController) getBlogByIdHandler(w http.ResponseWriter, r *http.Request) tmpl.View {
	blog := Blog{}
	id := r.URL.Query().Get("blog")
	err := controller.db.Where(id).Find(&blog).Error

	if err != nil {
		return tmpl.View{Status: 404, Error: err}
	}

	return tmpl.View{Name: "blog", Status: 200, Data: blog, Error: nil}
}

func (controller *BlogController) getBlogIndexHandler(w http.ResponseWriter, r *http.Request) tmpl.View {
	blogs := []Blog{}
	err := controller.db.Find(&blogs).Error
	if err != nil {
		return tmpl.View{Error: err}
	}

	return tmpl.View{Name: "blog_list", Status: 200, Data: blogs, Error: nil}
}

func (controller *BlogController) createNewBlogHandler(w http.ResponseWriter, r *http.Request) tmpl.View {
	blog := Blog{}

	err := r.ParseForm()
	if err != nil {
		log.Printf("Error white parsing form: %s", err.Error())
		return tmpl.View{Status: http.StatusNotAcceptable, Error: err}
	}

	u, err := auth.BasicAuthUserExtractor(r, routes.Ctx)
	if err != nil {
		log.Printf("Error white getting user: %s", err.Error())
		return tmpl.View{Status: http.StatusForbidden, Error: err}
	}

	blog.Title = r.Form.Get("title")
	blog.Subtitle = r.Form.Get("subtitle")
	blog.Content = template.HTML(r.Form.Get("text"))
	blog.Author = u.Name
	blog.PublishedOn = time.Now()
	err = controller.db.Save(&blog).Error
	if err != nil {
		log.Printf("Error white saving blog: %s", err.Error())
		return tmpl.View{Status: http.StatusNotAcceptable, Error: err}
	}

	return tmpl.View{Name: "success", Status: http.StatusCreated, Data: common.Status{Message: "Successfully created blog."}}
}

func (controller *BlogController) newBlogTemplateHandler(w http.ResponseWriter, r *http.Request) tmpl.View {
	return tmpl.View{Status: 200, Name: "new_blog"}
}
