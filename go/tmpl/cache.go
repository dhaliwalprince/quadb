package tmpl

import (
	"fmt"
	"html/template"
	"io"
	"log"
)

type Template struct {
	t     *template.Template
	ready bool
	files []string
	l     *log.Logger
	name  string
}

func (tmpl *Template) AddFiles(names []string) {
	if tmpl.ready {
		panic("new template cannot be added once after building compilation cache")
	}

	tmpl.files = append(tmpl.files, names...)
}

func NewTemplate(name string, l *log.Logger, names []string) *Template {
	tmpl := &Template{
		ready: false,
		l:     l,
		name:  name,
	}

	n := []string{}

	for _, name := range names {
		n = append(n, "templates/"+name+".tmpl")
	}

	tmpl.AddFiles(n)
	return tmpl
}

func (tmpl *Template) buildCache() error {
	t, err := template.ParseFiles(tmpl.files...)
	tmpl.t = t
	if err != nil {
		return err
	}

	tmpl.ready = true
	return nil
}

func (tmpl *Template) Execute(w io.Writer, data interface{}) error {
	if !tmpl.ready {
		t, err := template.ParseFiles(tmpl.files...)
		tmpl.t = t
		if err != nil {
			return err
		}
	}
	return tmpl.t.ExecuteTemplate(w, tmpl.name, data)
}

type TemplateCache struct {
	c map[string]*Template
}

func NewTemplateCache() *TemplateCache {
	return &TemplateCache{c: make(map[string]*Template)}
}

func (cache *TemplateCache) Add(tmpl *Template) {
	cache.c[tmpl.name] = tmpl
}

func (cache *TemplateCache) Get(name string) (*Template, error) {
	t, ok := cache.c[name]
	if !ok {
		return nil, fmt.Errorf("template %s doesn't exist", name)
	}

	return t, nil
}

func (cache *TemplateCache) Cache() error {
	for key, value := range cache.c {
		log.Printf("Caching %s...\n", key)
		err := value.buildCache()
		if err != nil {
			value.l.Println("Unable to build cache: ", err)
		}
	}

	return nil
}
