package tmpl

import (
	"bitbucket.org/dhaliwalprince/quadb/go/assets"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
)

type View struct {
	Vars   map[string]interface{}
	Data   interface{}
	Name   string
	Status int
	Error  error
}

type TemplateHandler func(w http.ResponseWriter, r *http.Request) View

type TemplateContext struct {
	cache       *TemplateCache
	mainAppJS   string
	mainAppRoot string
}

type ErrorData struct {
	Code    int
	Message string
}

func (ctx *TemplateContext) ExecuteErrorStatus(status int, w http.ResponseWriter, r *http.Request) {
	t404, err := ctx.cache.Get("error")
	if err != nil {
		log.Printf("%s Handler not found, falling back to native", status)
		w.Write([]byte(http.StatusText(status)))
		return
	}

	err = t404.Execute(w, ErrorData{Code: status, Message: http.StatusText(status)})
	if err != nil {
		log.Printf("Error while executing %s template: %v\n", status, err)
		return
	}
}

func (ctx *TemplateContext) MakeTemplateHandler(handler TemplateHandler) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		view := handler(w, r)
		if len(view.Name) == 0 {
			ctx.ExecuteErrorStatus(view.Status, w, r)
			return
		}
		t, err := ctx.cache.Get(view.Name)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			ctx.ExecuteErrorStatus(404, w, r)
			return
		}

		err = t.Execute(w, view.Data)
		if err != nil {
			log.Printf("Error while executing template: %s: %s", view.Name, err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			ctx.ExecuteErrorStatus(500, w, r)
		}
	})
}

func (ctx *TemplateContext) CreateTemplateHandler(name string) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		view, err := ctx.cache.Get(name)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			ctx.ExecuteErrorStatus(404, w, r)
			return
		}

		err = view.Execute(w, nil)
		if err != nil {
			log.Printf("Error white executing template: %s: %s", name, err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			ctx.ExecuteErrorStatus(500, w, r)
		}
	})
}

type ReactTemplateData struct {
	ReactRoot       string
	MainJSLocation  string `json:"main.js"`
	MainCSSLocation string `json:"main.css"`
}

func (ctx *TemplateContext) GetMainAppHandler() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		view, err := ctx.cache.Get("react")
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			ctx.ExecuteErrorStatus(500, w, r)
			return
		}

		f, err := assets.Open("mainapp/asset-manifest.json")
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			ctx.ExecuteErrorStatus(500, w, r)
			return
		}

		decoder := json.NewDecoder(f)
		data := ReactTemplateData{ReactRoot: "root"}
		err = decoder.Decode(&data)
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			ctx.ExecuteErrorStatus(500, w, r)
			return
		}

		data.MainJSLocation = path.Join("/cdn", "mainapp", data.MainJSLocation)
		data.MainCSSLocation = path.Join("/cdn", "mainapp", data.MainCSSLocation)
		err = view.Execute(w, data)

		if err != nil {
			log.Printf("Error while executing template: %s: %s", "react", err.Error())
		}
	})
}

func (ctx *TemplateContext) GetAdminAppHandler() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		view, err := ctx.cache.Get("admin")
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			ctx.ExecuteErrorStatus(500, w, r)
			return
		}

		f, err := assets.Open("admin/asset-manifest.json")
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			ctx.ExecuteErrorStatus(500, w, r)
			return
		}

		decoder := json.NewDecoder(f)
		data := ReactTemplateData{ReactRoot: "root"}
		err = decoder.Decode(&data)
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			ctx.ExecuteErrorStatus(500, w, r)
			return
		}

		data.MainJSLocation = path.Join("/cdn", "admin", data.MainJSLocation)
		data.MainCSSLocation = path.Join("/cdn", "admin", data.MainCSSLocation)
		err = view.Execute(w, data)

		if err != nil {
			log.Printf("Error while executing template: %s: %s", "admin", err.Error())
		}
	})
}
func NewTemplateContext(cache *TemplateCache) *TemplateContext {
	return &TemplateContext{cache: cache}
}

type templateDesc struct {
	Name       string   `json:"name"`
	Components []string `json:"components"`
}

func readTemplateDesc(path string) ([]templateDesc, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	descs := []templateDesc{}
	data, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(data, &descs)
	return descs, err
}

func cacheTemplates() *TemplateCache {
	cache := NewTemplateCache()
	l := log.New(os.Stdout, "quadb.in", log.Llongfile)

	descs, err := readTemplateDesc("templates/templates.json")
	if err != nil {
		panic(err)
	}

	for _, desc := range descs {
		tmpl := NewTemplate(desc.Name, l, desc.Components)
		cache.Add(tmpl)
	}
	return cache
}

var ctx *TemplateContext
var initialized = false

func GetTemplateContext() *TemplateContext {
	if !initialized {
		cache := cacheTemplates()
		ctx = NewTemplateContext(cache)
		initialized = true
	}
	return ctx
}
