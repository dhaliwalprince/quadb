package pages

import (
	"bitbucket.org/dhaliwalprince/quadb/go/models"
	"bitbucket.org/dhaliwalprince/quadb/go/tmpl"
	"github.com/jinzhu/gorm"
	"net/http"
)

type IndexData struct {
	LinkToDesigner string
	ProductList    []models.Category
}

type IndexController struct {
	DB *gorm.DB
}

var Data IndexData

func (controller *IndexController) Handler(w http.ResponseWriter, r *http.Request) tmpl.View {
	err := controller.DB.Find(&Data.ProductList).Error
	if err != nil {
		return tmpl.View{Status: 500, Error: err}
	}
	return tmpl.View{Name: "index", Status: 200, Data: Data}
}
func init() {
	Data.LinkToDesigner = "/launch-designer"
}
