package routes

import (
	"errors"
	"strconv"

	"bitbucket.org/dhaliwalprince/quadb/go/common"
	"bitbucket.org/dhaliwalprince/quadb/go/common/log"
	"bitbucket.org/dhaliwalprince/quadb/go/models"
	"bitbucket.org/dhaliwalprince/quadb/go/rest"
)

func GetAllCategoriesHandler(request *rest.ApiRequest) (interface{}, error) {
	return request.Context.Catalog.GetCategories()
}

func validateCategory(category models.Category) []error {
	errs := []error{}

	if len(category.Name) < 1 {
		errs = append(errs, errors.New("Name should not be empty"))
	}

	return errs
}

func AddCategoryHandler(request *rest.ApiRequest) (interface{}, error) {
	category := models.Category{}
	err := request.ParseBody(&category)
	if err != nil {
		return nil, errors.New("illegal request body")
	}

	errs := validateCategory(category)
	if len(errs) > 1 {
		return nil, rest.MultipleError{Errs: errs}
	}

	err = request.Context.Catalog.AddCategory(category)
	return common.Status{common.SUCCESS, "Successfully created category"}, err
}

func UpdateCategoryHandler(request *rest.ApiRequest) (interface{}, error) {
	category := models.Category{}
	err := request.ParseBody(&category)
	if err != nil {
		return nil, errors.New("illegal request body")
	}

	errs := validateCategory(category)
	if len(errs) > 1 {
		return nil, rest.MultipleError{Errs: errs}
	}

	err = request.Context.Catalog.AddCategory(category)
	return common.Status{common.SUCCESS, "Successfully updated category."}, err
}

func DeleteCategoryHandler(request *rest.ApiRequest) (interface{}, error) {
	id := request.Vars["id"]
	categoryId, _ := strconv.Atoi(id)
	category, err := request.Context.Catalog.GetCategoryById(categoryId)
	if err != nil {
		log.E("%v", err)
		return nil, errors.New("category doesn't exist")
	}

	err = request.Context.Catalog.RemoveCategory(*category)
	return common.Status{common.SUCCESS, "Successfully removed category."}, err
}

func GetAllMaterials(request *rest.ApiRequest) (interface{}, error) {
	return request.Context.Catalog.GetClothMaterials()
}

func AddMaterialHandler(request *rest.ApiRequest) (interface{}, error) {
	material := models.ClothMaterial{}
	err := request.ParseBody(&material)
	if err != nil {
		return nil, errors.New("illegal request body")
	}

	err = request.Context.Catalog.AddClothMaterial(material)
	return common.Status{common.SUCCESS, "Successfully saved material."}, err
}

func DeleteMaterialHandler(request *rest.ApiRequest) (interface{}, error) {
	id, _ := strconv.Atoi(request.Vars["id"])
	material, err := request.Context.Catalog.GetClothMaterialById(id)
	if err != nil {
		log.E("%v", err)
		return nil, errors.New("material not found")
	}

	err = request.Context.Catalog.RemoveClothMaterial(*material)
	return common.Status{common.SUCCESS, "Successfully deleted material."}, err
}

func UpdateMaterialHandler(request *rest.ApiRequest) (interface{}, error) {
	material := models.ClothMaterial{}
	err := request.ParseBody(&material)
	if err != nil {
		return nil, errors.New("illegal request body")
	}

	err = request.Context.Catalog.UpdateClothMaterial(material)
	return common.Status{common.SUCCESS, "Successfully updated material."}, err
}
