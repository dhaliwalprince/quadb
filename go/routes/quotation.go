package routes

import (
	"strconv"

	"bitbucket.org/dhaliwalprince/quadb/go/models"
	"bitbucket.org/dhaliwalprince/quadb/go/rest"
)

func ListQuotationHandlerForRequest(request *rest.ApiRequest) (interface{}, error) {
	requestId, _ := strconv.Atoi(request.Vars["requestId"])
	quotations, err := request.Context.Orders.GetQuotations(requestId)
	return quotations, err
}

func calculateSize(request models.OrderRequest) uint {
	q := uint(0)
	for _, size := range request.SizeList {
		q += size.Quantity
	}

	return q
}

func CreateQuotationHandler(request *rest.ApiRequest) (interface{}, error) {
	quotation := models.Quotation{}
	err := request.ParseBody(&quotation)
	if err != nil {
		return nil, err
	}

	r := models.OrderRequest{}
	err = request.Context.Config.GormDb.Preload("SizeList").
		Where("id = ?", quotation.RequestId).Find(&r).Error
	if err != nil {
		return nil, err
	}

	quotation.Quantity = calculateSize(r)
	err = request.Context.Orders.CreateQuotation(quotation)
	return rest.Success("Successfully created quotation."), err
}

func GetQuotationByIdHandler(request *rest.ApiRequest) (interface{}, error) {
	db := request.Context.Config.GormDb
	quotation := models.Quotation{}

	db = db.Preload("Request").Preload("ClothMaterial").Preload("Category").
		Preload("Request.Address").Preload("Request.SizeList").Preload("Request.Design")

	id, _ := strconv.Atoi(request.Vars["quotationId"])
	err := db.Find(&quotation, id).Error
	return quotation, err
}

func DeleteQuotationHandler(request *rest.ApiRequest) (interface{}, error) {
	quotationId, _ := strconv.Atoi(request.Vars["quotationId"])
	requestId, _ := strconv.Atoi(request.Vars["requestId"])
	quotation := models.Quotation{
		Model:     models.Model{ID: (uint)(quotationId)},
		RequestId: (uint)(requestId),
	}

	return rest.Success("Successfully deleted quotation"), request.Context.Orders.DeleteQuotation(quotation)
}
