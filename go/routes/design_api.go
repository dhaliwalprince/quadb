// this provides apis for storing, viewing and deleing user designs
package routes

import (
	"errors"
	"net/url"

	"bitbucket.org/dhaliwalprince/quadb/go/common"
	"bitbucket.org/dhaliwalprince/quadb/go/common/log"
	"bitbucket.org/dhaliwalprince/quadb/go/models"
	"bitbucket.org/dhaliwalprince/quadb/go/rest"
)

// returns all the designs made by all the users
// accessible to: admin
// limited to: 100
func GetAllDesignsHandler(request *rest.ApiRequest) (interface{}, error) {
	return request.Context.Designs.GetAllDesigns(0)
}

// returns all the designs made by user<id>
// accessible to: user, staff, admin
// limited to: 100
func GetDesignsByUser(request *rest.ApiRequest) (interface{}, error) {
	return request.Context.Designs.GetUserDesigns(request.GetUser(), 0)
}

// save the design
func SaveDesign(request *rest.ApiRequest) (interface{}, error) {
	design := models.Design{}
	err := request.ParseBody(&design)
	if err != nil {
		log.E("%v", err)
		return nil, errors.New("illegal request body")
	}

	if design.IsEmpty {
		return common.Status{common.SUCCESS, "successfully saved design"},
			request.Context.Designs.SaveDesign(request.GetUser(), design)
	}

	u, err := url.Parse(design.ImageUrl)
	if len(u.Hostname()) > 0 {
		return nil, errors.New("invalid image url")
	}

	return common.Status{common.SUCCESS, "successfully saved design"},
		request.Context.Designs.SaveDesign(request.GetUser(), design)
}

// delete the design
func DeleteDesign(request *rest.ApiRequest) (interface{}, error) {
	design := models.Design{}
	err := request.ParseBody(&design)
	if err != nil {
		return nil, errors.New("illegal request body")
	}

	if design.UserId != request.GetUser().ID {
		return nil, errors.New("user invalid")
	}

	return common.Status{common.SUCCESS, "successfully deleted address"}, request.Context.Designs.DeleteDesign(request.GetUser(), design)
}

func FindDesignByHashHandler(request *rest.ApiRequest) (interface{}, error) {
	hash := request.Vars["hash"]
	return request.Context.Designs.GetDesignByHash(request.GetUser(), hash)
}
