package routes

import (
	"errors"
	"strconv"
	"strings"

	"bitbucket.org/dhaliwalprince/quadb/go/common"
	"bitbucket.org/dhaliwalprince/quadb/go/common/log"
	"bitbucket.org/dhaliwalprince/quadb/go/models"
	"bitbucket.org/dhaliwalprince/quadb/go/rest"
)

func NewOrderEvent(t string, user *models.User, comments string) *models.OrderEvent {
	return &models.OrderEvent{
		Type:      t,
		UserId:    user.ID,
		Comments:  comments,
		Completed: false,
	}
}

func orderInHigherStage(order *models.Order, t string) bool {
	if len(order.Events) < 2 {
		return false
	}

	for _, event := range order.Events {
		if strings.Compare(event.Type, t) >= 0 {
			return true
		}
	}

	return false
}

func orderInStage(order *models.Order, t string) bool {
	for _, event := range order.Events {
		if strings.Compare(event.Type, t) == 0 {
			return true
		}
	}
	return false
}

func PlaceOrderInCart(request *rest.ApiRequest) (interface{}, error) {
	user := request.GetUser()
	order := models.Order{}
	err := request.ParseBody(&order)

	if err != nil {
		return nil, errors.New("unable to parse body")
	}

	order.Events = []models.OrderEvent{}
	order.Events = append(order.Events, *NewOrderEvent(models.ORDER_IN_CART, user, ""))

	err = request.Context.Orders.SaveOrder(user, order)
	return common.Status{Status: common.SUCCESS, Message: "Successfully saved order."}, err
}

func GetUserOrdersHandler(request *rest.ApiRequest) (interface{}, error) {
	orders, err := request.Context.Orders.GetOrdersOfUser(request.GetUser())
	if err != nil {
		log.W("error: %v", err)
		return nil, err
	}

	return orders, err
}

func GetOrderByIdHandler(request *rest.ApiRequest) (interface{}, error) {
	id, _ := strconv.Atoi(request.Vars["id"])
	return request.Context.Orders.GetOrderBy(request.GetUser(), (uint)(id))
}

func PlaceOrderHandler(request *rest.ApiRequest) (interface{}, error) {
	orderId := request.Vars["id"]
	id, err := strconv.Atoi(orderId)
	addrId := request.GetHttpRequest().URL.Query().Get("addressId")
	if err != nil {
		return nil, errors.New("invalid order id")
	}

	addressId, err := strconv.Atoi(addrId)
	if len(addrId) < 1 || err != nil {
		return nil, errors.New("invalid/missing address")
	}

	order, err := request.Context.Orders.GetOrderBy(request.GetUser(), (uint)(id))
	if err != nil {
		return nil, errors.New("could not the specified error")
	}

	address, err := request.Context.Addresses.FindBy(request.GetUser(), addressId)

	if err != nil {
		return nil, errors.New("could not find specified address")
	}

	order.OrderDetails.AddressId = address.ID

	if orderInHigherStage(order, models.ORDER_PLACED) {
		return nil, errors.New("order can not be modified now")
	}

	err = request.Context.Orders.UpdateOrderAddress(order)
	if err != nil {
		return nil, errors.New("could not place order")
	}

	orderInfo := NewOrderEvent(models.ORDER_PLACED, request.GetUser(), "")
	orderInfo.OrderId = order.ID
	err = request.Context.Orders.DB.Model(order).Save(orderInfo).Error
	return common.Status{common.SUCCESS, "Successfully placed order."}, err
}

func CancelOrderHandler(request *rest.ApiRequest) (interface{}, error) {
	id := request.Vars["id"]
	orderId, _ := strconv.Atoi(id)
	order, err := request.Context.Orders.GetOrderBy(request.GetUser(), (uint)(orderId))
	if err != nil {
		return nil, errors.New("order not found")
	}
	if orderInHigherStage(order, models.ORDER_ACCEPTED) {
		return nil, errors.New("order cannot be cancelled now")
	}
	cancelEvent := NewOrderEvent(models.ORDER_CANCELLED, request.GetUser(), "order cancelled by user")
	cancelEvent.OrderId = order.ID
	err = request.Context.Orders.DB.Model(order).Save(cancelEvent).Error
	return common.Status{common.SUCCESS, "Successfully cancelled order."}, err
}

func RejectOrderByStaffHandler(request *rest.ApiRequest) (interface{}, error) {
	id := request.Vars["orderId"]
	orderId, _ := strconv.Atoi(id)
	id = request.Vars["userId"]
	userId, _ := strconv.Atoi(id)
	user, err := request.Context.Users.GetUserById(userId)
	if err != nil {
		return nil, errors.New("user not found")
	}
	order, err := request.Context.Orders.GetOrderBy(user, (uint)(orderId))
	if err != nil {
		return nil, errors.New("order not found")
	}
	cancelEvent := NewOrderEvent(models.ORDER_REJECTED, request.GetUser(), "order rejected")
	cancelEvent.OrderId = order.ID
	err = request.Context.Orders.DB.Model(order).Save(cancelEvent).Error
	return common.Status{common.SUCCESS, "Successfully rejected order."}, err
}

func MarkOrderDeliveredHandler(request *rest.ApiRequest) (interface{}, error) {
	id := request.Vars["orderId"]
	orderId, _ := strconv.Atoi(id)
	id = request.Vars["userId"]
	userId, _ := strconv.Atoi(id)
	user, err := request.Context.Users.GetUserById(userId)
	if err != nil {
		return nil, errors.New("user not found")
	}
	order, err := request.Context.Orders.GetOrderBy(user, (uint)(orderId))
	if err != nil {
		return nil, errors.New("order not found")
	}
	cancelEvent := NewOrderEvent(models.ORDER_DELIVERED, request.GetUser(), "order delivered")
	cancelEvent.OrderId = order.ID
	err = request.Context.Orders.DB.Model(order).Save(cancelEvent).Error
	return common.Status{common.SUCCESS, "Successfully marked delivery order."}, err
}

func UpdateOrderInfo(request *rest.ApiRequest) (interface{}, error) {
	info := models.OrderDetails{}
	err := request.ParseBody(&info)
	if err != nil {
		return nil, errors.New("illegal request body")
	}

	id, _ := strconv.Atoi(request.Vars["id"])
	order, err := request.Context.Orders.GetOrderBy(request.GetUser(), (uint)(id))
	if err != nil {
		return nil, errors.New("order not found")
	}

	if orderInHigherStage(order, models.ORDER_PLACED) {
		return nil, errors.New("order cannot be edited after placing")
	}

	// can only update the quantity
	err = request.Context.Orders.UpdateOrderInfo(order, info)
	return common.Status{common.SUCCESS, "Successfully updated order."}, err
}

func AcceptOrderHandler(request *rest.ApiRequest) (interface{}, error) {
	id := request.Vars["orderId"]
	orderId, _ := strconv.Atoi(id)
	id = request.Vars["userId"]
	userId, _ := strconv.Atoi(id)
	user, err := request.Context.Users.GetUserById(userId)
	if err != nil {
		return nil, errors.New("user not found")
	}
	order, err := request.Context.Orders.GetOrderBy(user, (uint)(orderId))
	if err != nil {
		return nil, errors.New("order not found")
	}

	if orderInStage(order, models.ORDER_CANCELLED) || !orderInStage(order, models.ORDER_PLACED) {
		return nil, errors.New("order either not placed or has been cancelled")
	}
	event := NewOrderEvent(models.ORDER_ACCEPTED, request.GetUser(), "order accepted")
	event.OrderId = order.ID
	err = request.Context.Orders.DB.Model(order).Save(event).Error
	return common.Status{common.SUCCESS, "Successfully accepted order."}, err
}

func RemoveFromCartHandler(request *rest.ApiRequest) (interface{}, error) {
	id := request.Vars["id"]
	orderId, _ := strconv.Atoi(id)

	order, err := request.Context.Orders.GetOrderBy(request.GetUser(), (uint)(orderId))
	if err != nil {
		return nil, errors.New("record not found")
	}

	if orderInHigherStage(order, models.ORDER_PLACED) {
		return nil, errors.New("order cannot be altered now")
	}

	err = request.Context.Orders.DeleteOrder(request.GetUser(), *order)
	return common.Status{common.SUCCESS, "Successfully deleted item."}, err
}

func ListAllOrders(request *rest.ApiRequest) (interface{}, error) {
	filter := request.GetHttpRequest().URL.Query().Get("filter")
	if len(filter) > 1 {
		switch filter {
		case "accepted":
			return request.Context.Orders.GetAcceptedOrders()
		case "created":
			return request.Context.Orders.GetNewOrders()
		case "rejected":
			return request.Context.Orders.GetRejectedOrders()
		case "cancelled":
			return request.Context.Orders.GetCancelledOrders()
		case "delivered":
			return request.Context.Orders.GetDeliveredOrders()
		}

	}
	return request.Context.Orders.GetAllOrders()
}

func FindOrderHandler(request *rest.ApiRequest) (interface{}, error) {
	return request.Context.Orders.FindOrderBy(request.Vars["id"])
}

func RequestOrderHandler(request *rest.ApiRequest) (interface{}, error) {
	orderRequest := models.OrderRequest{}
	err := request.ParseBody(&orderRequest)
	if err != nil {
		return nil, errors.New("illegal request body")
	}

	quantity := uint(0)
	for _, size := range orderRequest.SizeList {
		quantity += size.Quantity
	}

	if quantity == 0 {
		return nil, errors.New("Quantity should not be 0.")
	}

	orderRequest.Status = "reviewing"
	err = request.Context.Orders.CreateRequest(request.GetUser(), orderRequest)
	return common.Status{common.SUCCESS, "Successfully created request."}, err
}

func ListUserRequests(request *rest.ApiRequest) (interface{}, error) {
	return request.Context.Orders.ListUserRequests(request.GetUser())
}

func ListAllRequests(request *rest.ApiRequest) (interface{}, error) {
	filter := request.GetHttpRequest().URL.Query().Get("filter")
	return request.Context.Orders.ListAllRequests(filter)
}

func UpdateRequestQuantityHandler(request *rest.ApiRequest) (interface{}, error) {
	m := map[string]uint{}
	err := request.ParseBody(&m)
	if err != nil {
		return nil, errors.New("illegal request body")
	}
	quantity, ok := m["quantity"]
	if !ok || quantity <= 0 {
		return nil, errors.New("quantity is not a valid positive integer")
	}

	requestId, ok := m["requestId"]
	if !ok || requestId <= 0 {
		return nil, errors.New("requestId is not valid")
	}

	err = request.Context.Orders.UpdateRequestQuantity(request.GetUser(), requestId, quantity)
	return rest.Success("Successfully created "), err
}

func UpdateRequestStatusHandler(request *rest.ApiRequest) (interface{}, error) {
	m := map[string]string{}
	err := request.ParseBody(&m)

	if err != nil {
		return nil, errors.New("illegal request body")
	}

	status, ok := m["status"]
	if !ok {
		return nil, errors.New("invalid status")
	}

	requestId, err := strconv.Atoi(m["requestId"])
	if err != nil {
		log.W("%v", err)
		return nil, errors.New("invalid request id")
	}

	err = request.Context.Orders.UpdateRequestStatus(request.GetUser(), (uint)(requestId), status)
	return rest.Success("Successfully updated"), err
}

func GetUserRequestHandler(request *rest.ApiRequest) (interface{}, error) {
	requestId, _ := strconv.Atoi(request.Vars["requestId"])
	orderRequest := models.OrderRequest{}

	db := request.Context.Config.GormDb.Preload("Address").Preload("SizeList").Preload("Design")
	err := db.Where(requestId).First(&orderRequest).Error
	if err != nil {
		log.W("%v", err)
		return nil, rest.NotFound{}
	}

	return orderRequest, err
}

func CancelRequestHandler(request *rest.ApiRequest) (interface{}, error) {
	requestId, _ := strconv.Atoi(request.Vars["requestId"])
	orderRequest := models.OrderRequest{}

	err := request.Context.Orders.DB.Where(requestId).First(&orderRequest).Error
	if err != nil {
		log.W("%v", err)
		return nil, rest.NotFound{}
	}

	if orderRequest.Status != "reviewing" {
		return nil, errors.New("cannot change status now")
	}

	err = request.Context.Designs.DB.Model(&orderRequest).Update("status", "cancelled").Error
	return orderRequest, err
}

func ChangeRequestStatus(request *rest.ApiRequest) (interface{}, error) {
	requestId, _ := strconv.Atoi(request.Vars["requestId"])
	orderRequest := models.OrderRequest{}

	err := request.Context.Orders.DB.Where(requestId).First(&orderRequest).Error
	if err != nil {
		log.W("%v", err)
		return nil, rest.NotFound{}
	}

	status := request.GetHttpRequest().URL.Query().Get("status")
	err = request.Context.Designs.DB.Model(&orderRequest).Update("status", status).Error
	return orderRequest, err
}

func GetRequestByIdHandler(request *rest.ApiRequest) (interface{}, error) {
	requestId, _ := strconv.Atoi(request.Vars["requestId"])
	orderRequest := models.OrderRequest{}

	err := request.Context.Orders.DB.Preload("Design").Preload("Address").Preload("SizeList").Where(requestId).First(&orderRequest).Error
	if err != nil {
		log.W("%v", err)
		return nil, rest.NotFound{}
	}
	return orderRequest, err
}
