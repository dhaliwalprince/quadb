package routes

import (
	"errors"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"path"

	"bitbucket.org/dhaliwalprince/quadb/go/context"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

type WebsiteVars struct {
	IndexJS  string
	IndexCSS string
	Extras   map[string]string
}

type ContentServer struct {
	t         *template.Template
	buildDir  string
	Vars      WebsiteVars
	AdminVars WebsiteVars
	excludes  []string
	root      string
}

func (server *ContentServer) exists(p string) bool {
	stat, err := os.Stat(server.path(p))
	if err != nil {
		log.Printf("%s doesn't exist: %v", p, err)
		return false
	}

	return stat.IsDir()
}

func (server *ContentServer) path(p string) string {
	return path.Clean(path.Join(server.buildDir, p))
}

func (server *ContentServer) httpHandler(w http.ResponseWriter, r *http.Request) {
	rest := mux.Vars(r)["rest"]
	log.Println(rest)
	if server.exists(r.RequestURI) {
		http.ServeFile(w, r, server.path(r.RequestURI))
		return
	}

	err := server.t.Execute(w, server.Vars)
	if err != nil {
		log.Printf("Error occurred while executing template: %v", err)
	}
}

func (server *ContentServer) addIndexFile(path string) {
	server.t = template.Must(template.ParseFiles(path))
}

func (server *ContentServer) addBuildDirectory(dir string) error {
	stat, err := os.Stat(dir)
	if err != nil {
		return err
	}

	if !stat.IsDir() {
		return errors.New(fmt.Sprintf("%s is not a directory.", dir))
	}

	server.buildDir = dir
	return nil
}

func (server *ContentServer) AddVar(key, value string) {
	server.Vars.Extras[key] = value
}

func (server *ContentServer) initialize(db *gorm.DB) error {
	return nil
}

func (server *ContentServer) install(mux *http.ServeMux) {
	mux.HandleFunc("/{rest:[.]*}", server.httpHandler)
}

func NewContentServer(buildDir string, router *http.ServeMux) *ContentServer {
	server := ContentServer{}
	server.addBuildDirectory(buildDir)
	server.install(router)

	server.addIndexFile("./templates/index.tmpl")
	for _, asset := range context.Config.AdminAssets {
		folder, _ := asset["folder"]
		file, _ := asset["name"]
		if path.Ext(file) == ".js" {
			server.AdminVars.IndexJS = path.Join(folder, file)
		} else {
			server.AdminVars.IndexCSS = path.Join(folder, file)
		}
	}

	log.Printf("Found admin assets: %v", server.AdminVars)
	for _, asset := range context.Config.AppAssets {
		folder, _ := asset["folder"]
		file, _ := asset["name"]
		if path.Ext(file) == ".js" {
			server.Vars.IndexJS = path.Join(folder, file)
		} else {
			server.Vars.IndexCSS = path.Join(folder, file)
		}
	}
	log.Printf("Found app assets: %v", server.Vars)
	return &server
}
