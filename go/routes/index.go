package routes

import (
	"bitbucket.org/dhaliwalprince/quadb/go/auth"
	"bitbucket.org/dhaliwalprince/quadb/go/context"
	"bitbucket.org/dhaliwalprince/quadb/go/pages"
	"bitbucket.org/dhaliwalprince/quadb/go/tmpl"
	"log"
	"net/http"
	"net/url"
	"time"
)

var TemplateContext *tmpl.TemplateContext

func SetTemplateContext(ctx *tmpl.TemplateContext) {
	TemplateContext = ctx
}

func LogoutHandler(w http.ResponseWriter, r *http.Request, successHandler http.HandlerFunc) {
	cookie, err := r.Cookie("token")
	if err != nil {
		successHandler(w, r)
		return
	}

	cookie.Expires = time.Now().Add(-1 * time.Second)
	http.SetCookie(w, cookie)

	cont := r.URL.Query().Get("continueto")
	if len(cont) > 0 {
		u, err := url.QueryUnescape(cont)
		if err != nil {
			log.Println(err)
		} else {
			log.Println(u)
		}
	}
	successHandler(w, r)
}

func RootHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path == "/" {
		controller := pages.IndexController{DB: context.Config.GormDb}
		handler := TemplateContext.MakeTemplateHandler(controller.Handler)
		handler(w, r)
		return
	} else if r.URL.Path == "/maintenance" {
		handler := TemplateContext.CreateTemplateHandler("under_maintenance")
		handler(w, r)
		return
	} else if r.URL.Path == "/logout" {
		LogoutHandler(w, r, TemplateContext.GetMainAppHandler())
	} else {
		handler := TemplateContext.GetMainAppHandler()
		handler(w, r)
		return
	}
}

func AdminRootHandler(w http.ResponseWriter, r *http.Request) {
	user, err := auth.CookieAuthExtractor(r, Ctx)
	if err != nil || !auth.HasRole(Ctx, user, "ADMIN") || !auth.HasRole(Ctx, user, "STAFF") {
		log.Println(err)
		q := (url.QueryEscape("a=1&continueto=/admin"))
		http.Redirect(w, r, "/logout?continueto=/login?"+q, http.StatusSeeOther)
		return
	}

	handler := TemplateContext.GetAdminAppHandler()
	handler(w, r)
}
