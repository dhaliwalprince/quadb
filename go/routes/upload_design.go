package routes

import (
	"bytes"
	"errors"
	"strconv"

	"bitbucket.org/dhaliwalprince/quadb/go/common/log"
	"bitbucket.org/dhaliwalprince/quadb/go/context"
	"bitbucket.org/dhaliwalprince/quadb/go/models"
	"bitbucket.org/dhaliwalprince/quadb/go/rest"
	"github.com/jinzhu/gorm"
	"github.com/princedhaliwal/go-photos"
)

type UploadService struct {
	photos       photos.PhotoService
	AssetsFolder string
}

type ImageStore struct {
	db *gorm.DB
}

func NewImageStore(db *gorm.DB) ImageStore {
	return ImageStore{db: db}
}

func (service *UploadService) InitializeService(config *context.Configuration) (err error) {
	config.GormDb.AutoMigrate(&photos.Photo{}, &photos.Album{})
	service.photos, err = photos.NewPhotoService(config.ObjectStoreUrl, config.AwsAccessKey,
		config.AwsSecretKey, &ImageStore{db: config.GormDb}, config.Bucket)
	return
}

func (store *ImageStore) Save(object interface{}) error {
	return store.db.Save(object).Error
}

func (store *ImageStore) FindPhoto(id uint) (*photos.Photo, error) {
	photo := photos.Photo{}
	err := store.db.Find(&photo, id).Error
	return &photo, err
}

func (store *ImageStore) FindAlbum(name string) (*photos.Album, error) {
	album := photos.Album{}
	err := store.db.Find(&album, "name = ?", name).Error
	return &album, err
}

func (store *ImageStore) GetPhotosOfAlbum(album *photos.Album) ([]photos.Photo, error) {
	ps := []photos.Photo{}
	err := store.db.Model(album).Related(&ps).Error
	return ps, err
}

func getAlbumName(user *models.User) string {
	return strconv.Itoa((int)(user.ID))
}

func (service UploadService) UploadImage(context *rest.ApiRequest, albumName string) (interface{}, error) {
	var imageJson map[string]string
	err := context.ParseBody(&imageJson)
	if err != nil {
		return nil, err
	}

	b64Image, ok := imageJson["base64_image"]
	if !ok {
		return nil, errors.New("missing field base64_image")
	}

	name, ok := imageJson["name"]
	if !ok {
		return nil, errors.New("missing field name")
	}

	album, err := service.photos.GetAlbum(albumName)
	if err != nil {
		err = service.photos.CreateAlbum(albumName)
		if err != nil {
			log.W("%v", err)
			return nil, errors.New("unable to save image")
		}
		album = &photos.Album{Name: albumName}
	}
	photo, err := service.photos.SavePhoto(bytes.NewBufferString(b64Image), album)
	if err != nil {
		log.W("error while uploading: %v", err)
		return nil, errors.New("there were some technical issues while uploading design")
	}

	design := models.Design{Name: name, ImageUrl: photo.Hash, UserId: context.GetUser().ID, IsEmpty: false, IsPublic: false}
	err = context.Context.Designs.SaveDesign(context.GetUser(), design)
	res := map[string]string{}
	res["name"] = name
	res["id"] = strconv.Itoa((int)(design.ID))
	res["hash"] = photo.Hash[:len(photo.Hash)-4]
	res["url"] = photo.Hash
	return res, err
}

func (service UploadService) UploadPublicImage(context *rest.ApiRequest) (interface{}, error) {
	return service.UploadImage(context, service.AssetsFolder)
}

func (service UploadService) UploadUserImage(context *rest.ApiRequest) (interface{}, error) {
	return service.UploadImage(context, getAlbumName(context.GetUser()))
}

func (service UploadService) GetImage(context *rest.ApiRequest, albumName string) (interface{}, error) {
	album, err := service.photos.GetAlbum(albumName)
	if err != nil {
		return nil, rest.NotFound{}
	}

	image := context.Vars["image"]
	imReader, err := service.photos.GetPhoto(album, image)
	if err != nil {
		log.W("error: %v", err)
		return nil, errors.New("some internal error occurred")
	}

	return rest.ResponseStream{Stream: imReader, ContentType: "image/png"}, err
}

func (service UploadService) GetPublicImage(context *rest.ApiRequest) (interface{}, error) {
	return service.GetImage(context, service.AssetsFolder)
}

func (service UploadService) GetUserImage(context *rest.ApiRequest) (interface{}, error) {
	return service.GetImage(context, getAlbumName(context.GetUser()))
}
