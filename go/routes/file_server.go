package routes

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/url"

	"bitbucket.org/dhaliwalprince/quadb/go/auth"
	"bitbucket.org/dhaliwalprince/quadb/go/context"
	"bitbucket.org/dhaliwalprince/quadb/go/models"
	"bitbucket.org/dhaliwalprince/quadb/go/rest"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

type FileServer struct {
	db    *gorm.DB
	store *FileStore
}

func (server *FileServer) getFile(url string) (io.Reader, error) {
	file := models.File{}
	err := server.db.Where("url = ?", url).Find(&file).Error
	if err != nil {
		return nil, err
	}
	return server.store.Get(&file)
}

func (server *FileServer) save(file *models.File, reader io.Reader) error {
	err := server.store.Save(file, reader)
	if err != nil {
		return err
	}

	return server.db.Save(file).Error
}

func (server *FileServer) getFileHandler(request *rest.ApiRequest) (interface{}, error) {
	id, ok := request.Vars["name"]
	if !ok {
		return nil, rest.NotFound{}
	}

	return server.getFile(id)
}

func (server *FileServer) uploadFileHandler(request *rest.ApiRequest) (interface{}, error) {
	err := request.GetHttpRequest().ParseMultipartForm(1024 * 1024)
	if err != nil {
		log.Printf("error while parsing form: %v\n", err)
		return nil, rest.UnauthorizedError{}
	}

	file, header, err := request.GetHttpRequest().FormFile("file[]")
	if err != nil {
		log.Printf("error while reading file[] field: %v\n", err)
		return nil, rest.ServerError{}
	}

	log.Printf("Found attachment, %s of size %v\n", header.Filename, header.Size)
	f := models.File{}
	f.Name = header.Filename
	f.OwnedBy = request.GetUser().ID

	f.Url = (fmt.Sprintf("/api/filestore/file/%s", url.PathEscape(f.Name)))
	data, err := ioutil.ReadAll(file)
	if err != nil {
		log.Printf("Error occurred while reading file. %v\n", err)
		return nil, rest.ServerError{}
	}

	log.Printf(string(data))

	err = server.save(&f, bytes.NewBuffer(data))
	if err != nil {
		log.Printf("error while saving file %s: %v\n", f.Name, err)
		return nil, rest.ServerError{}
	}

	return rest.Success("Successfully uploaded file"), nil
}

func (server *FileServer) deleteFileHandler(request *rest.ApiRequest) (interface{}, error) {
	id := request.Vars["name"]
	files := []models.File{}
	err := server.db.Where("name = ?", id).Find(&files).Error
	if err != nil {
		return nil, rest.NotFound{}
	}
	for _, file := range files {
		err = server.store.Delete(&file)
		if err != nil {
			return nil, rest.ServerError{}
		}
		err = server.db.Delete(&file).Error
		if err != nil {
			return nil, rest.ServerError{}
		}
	}
	return rest.Success("Successfully deleted file(s)."), nil
}

func (server *FileServer) getAllFileListHandler(request *rest.ApiRequest) (interface{}, error) {
	files := []models.File{}
	err := server.db.Find(&files).Error
	if err != nil {
		return nil, rest.ServerError{}
	}

	return files, err
}

func (server *FileServer) InstallHttpHandlers(router *mux.Router, ctx *rest.Context) {
	router.HandleFunc("/filestore/upload", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(server.uploadFileHandler), ctx)).Methods("POST")
	router.HandleFunc("/filestore/file/{name}", rest.MakeGenericApiHandler((server.getFileHandler), ctx)).Methods("GET")
	router.HandleFunc("/filestore/files", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(server.getAllFileListHandler), ctx)).Methods("GET")
	router.HandleFunc("/filestore/file/{name}", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(server.deleteFileHandler), ctx)).Methods("DELETE")
}

func NewFileServer(config *context.Configuration) *FileServer {
	store, _ := New(config.ObjectStoreUrl, config.AwsAccessKey, config.AwsSecretKey, "web-content")
	server := &FileServer{store: store, db: config.GormDb}
	return server
}
