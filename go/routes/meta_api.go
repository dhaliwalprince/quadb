package routes

import (
	"fmt"
	"log"
	"reflect"
	"strconv"
	"strings"

	"bitbucket.org/dhaliwalprince/quadb/go/auth"
	"bitbucket.org/dhaliwalprince/quadb/go/common"
	"bitbucket.org/dhaliwalprince/quadb/go/models"
	"bitbucket.org/dhaliwalprince/quadb/go/rest"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

type MetaApi struct {
	Name       string
	PathPrefix string
	Model      interface{}
	Meta       models.Meta
	DB         *gorm.DB
}

func CreateSlice(t reflect.Type) interface{} {
	slice := reflect.New(reflect.SliceOf(t))
	return slice.Interface()
}

func CreateElement(t reflect.Type) interface{} {
	value := reflect.New(t.Elem())
	return value.Interface()
}

func (api *MetaApi) GetAll(r *rest.ApiRequest) (interface{}, error) {
	resultSet := CreateSlice(reflect.TypeOf(api.Model))
	err := api.DB.Find(resultSet).Error
	return resultSet, err
}

func (api *MetaApi) GetOne(r *rest.ApiRequest) (interface{}, error) {
	id, _ := strconv.Atoi(r.Vars["id"])
	value := CreateElement(reflect.TypeOf(api.Model))
	err := api.DB.Where(id).First(value).Error
	return value, err
}

func (api *MetaApi) Create(r *rest.ApiRequest) (interface{}, error) {
	value := CreateElement(reflect.TypeOf(api.Model))
	err := r.ParseBody(value)
	if err != nil {
		return nil, err
	}
	log.Println(value)
	err = api.DB.Save(value).Error
	return rest.Success(fmt.Sprintf("Successfully saved '%s'", api.Name)), err
}

func (api *MetaApi) Update(r *rest.ApiRequest) (interface{}, error) {
	value := CreateElement(reflect.TypeOf(api.Model))
	err := r.ParseBody(value)
	if err != nil {
		return nil, err
	}
	id, _ := strconv.Atoi(r.Vars["id"])
	old := CreateElement(reflect.TypeOf(api.Model))
	err = api.DB.Where(id).First(old).Error
	if err != nil {
		return nil, err
	}
	err = api.DB.Model(old).Updates(value).Error
	return rest.Success(fmt.Sprintf("Successfully updated '%s'", api.Name)), err
}

func (api *MetaApi) Delete(r *rest.ApiRequest) (interface{}, error) {
	id, _ := strconv.Atoi(r.Vars["id"])
	old := CreateElement(reflect.TypeOf(api.Model))
	log.Println("Type: " + reflect.TypeOf(old).String())
	err := api.DB.Where(id).First(old).Error
	if err != nil {
		return nil, err
	}
	err = api.DB.Delete(old).Error
	return rest.Success(fmt.Sprintf("Successfully deleted '%s'", api.Name)), err
}

func InstallForStaff(api *MetaApi, router *mux.Router, ctx *rest.Context) {
	r := router.PathPrefix("/" + api.PathPrefix).Subrouter()
	r.HandleFunc("", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(api.GetAll), ctx)).Methods("GET")
	r.HandleFunc("/{id}", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(api.GetOne), ctx)).Methods("GET")
	r.HandleFunc("", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(api.Create), ctx)).Methods("POST")
	r.HandleFunc("/{id}", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(api.Update), ctx)).Methods("PUT")
	r.HandleFunc("/{id}", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(api.Delete), ctx)).Methods("DELETE")
}

type MetaApiManager struct {
	apis []*MetaApi
}

func (manager *MetaApiManager) GetAllMetas(r *rest.ApiRequest) (interface{}, error) {
	metas := []models.Meta{}

	for _, api := range manager.apis {
		metas = append(metas, api.Meta)
	}

	return metas, nil
}

func (manager *MetaApiManager) GetMetaById(r *rest.ApiRequest) (interface{}, error) {
	id, _ := strconv.Atoi(r.Vars["id"])
	return manager.apis[id-1].Meta, nil
}

func getType(t reflect.Type) string {
	switch t.Kind() {
	case reflect.Int:
	case reflect.Uint:
		return "number"

	case reflect.String:
		return "text"

	case reflect.Bool:
		return "boolean"

	case reflect.Float64:
		return "float"
	default:
		return "text"
	}
	return "text"
}

func getColumns(model interface{}) []models.Column {
	columns := []models.Column{}
	value := reflect.ValueOf(model).Elem()

	for i := 0; i < value.Type().NumField(); i++ {
		field := value.Type().Field(i)

		if field.Type.Kind() == reflect.Struct || field.Type.Kind() == reflect.Array {
			continue
		}

		editable := field.Type.Kind() == reflect.Uint && field.Type.Name() == "ID"

		t := getType(field.Type)
		column := models.Column{
			ID:           (uint)(i + 1),
			Name:         field.Name,
			Key:          common.Underscore(field.Name),
			Editable:     editable,
			Creatable:    editable,
			Required:     editable,
			Type:         t,
			IsPrimaryKey: field.Type.Name() == "ID",
			IsForeignKey: strings.HasSuffix(field.Type.Name(), "Id"),
		}

		columns = append(columns, column)
	}
	return columns
}

func (manager *MetaApiManager) CreateApi(name, pathPrefix string, model interface{}, ctx *rest.Context, preloads ...string) {
	db := ctx.Config.GormDb

	for _, preload := range preloads {
		log.Println("Preloading " + preload)
		db = db.Preload(preload)
	}

	meta := models.Meta{
		ID:       (uint)(len(manager.apis) + 1),
		Name:     name,
		Class:    reflect.TypeOf(model).PkgPath() + "." + reflect.TypeOf(model).Name(),
		Headline: name,
		Url:      pathPrefix,
		Urls: []models.Action{
			{Type: "GET", Endpoint: "/api/" + pathPrefix},
			{Type: "POST", Endpoint: "/api/" + pathPrefix},
			{Type: "PUT", Endpoint: "/api/" + pathPrefix},
			{Type: "DELETE", Endpoint: "/api/" + pathPrefix},
		},
		Columns: getColumns(model),
	}
	manager.apis = append(manager.apis, &MetaApi{Name: name, PathPrefix: pathPrefix, Model: model, DB: db, Meta: meta})
}

func (manager *MetaApiManager) install() {
	ApiRouter.HandleFunc("/meta", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(manager.GetAllMetas), Ctx)).Methods("GET")
	ApiRouter.HandleFunc("/meta/{id:[0-9]+}", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(manager.GetMetaById), Ctx)).Methods("GET")
}

func (manager *MetaApiManager) InstallApis() {
	for _, api := range manager.apis {
		InstallForStaff(api, ApiRouter, Ctx)
	}

	manager.install()
}

func InitMetaApis() {
	manager := MetaApiManager{}
	for _, model := range models.Models {
		name := reflect.TypeOf(model).Elem().Name()
		manager.CreateApi(name, "config/"+common.Underscore(name), model, Ctx)
	}

	manager.InstallApis()
}
