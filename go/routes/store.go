package routes

import (
	"io"
	"log"

	"bitbucket.org/dhaliwalprince/quadb/go/models"
	"github.com/minio/minio-go"
	"github.com/satori/go.uuid"
)

type FileStore struct {
	minioClient *minio.Client
	bucketName  string
}

func New(url, accessKey, secret, bucketName string) (store *FileStore, err error) {
	store = &FileStore{}
	store.bucketName = bucketName
	store.minioClient, err = minio.New(url, accessKey, secret, false)
	if err == nil {
		err = store.minioClient.MakeBucket(bucketName, "us-east-2")
		if err != nil {
			if r, err := store.minioClient.BucketExists(bucketName); r && err != nil {
				log.Printf("Bucket %s already exists!", bucketName)
			} else {
				log.Printf("Unable to make bucket %s: %v", bucketName, err)
			}
		}
	} else if err != nil {
		log.Println("Error while connecting to minio client: \n", err)
	}
	return
}

func (store *FileStore) Save(file *models.File, reader io.Reader) error {
	u, err := uuid.NewV4()
	if err != nil {
		return err
	}
	_, err = store.minioClient.PutObject(store.bucketName, u.String(), reader, -1, minio.PutObjectOptions{})
	if err != nil {
		return err
	}

	file.MinioId = u.String()
	return nil
}

func (store *FileStore) Get(file *models.File) (reader io.Reader, err error) {
	obj, err := store.minioClient.GetObject(store.bucketName, file.MinioId, minio.GetObjectOptions{})
	return obj, err
}

func (store *FileStore) Delete(file *models.File) error {
	return store.minioClient.RemoveObject(store.bucketName, file.MinioId)
}
