package routes

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"strconv"

	"bitbucket.org/dhaliwalprince/quadb/go/common"
	"bitbucket.org/dhaliwalprince/quadb/go/models"
	"bitbucket.org/dhaliwalprince/quadb/go/rest"
	"github.com/asaskevich/govalidator"
	"golang.org/x/crypto/bcrypt"
)

func GetUsersHandler(request *rest.ApiRequest) (interface{}, error) {
	return request.Context.Users.GetAll()
}

func GetUserByIdHandler(request *rest.ApiRequest) (interface{}, error) {
	id, err := strconv.Atoi(request.Vars["id"])

	if err != nil {
		return nil, errors.New("invalid request")
	}

	user, err := request.Context.Users.GetUserById(id)

	if err != nil {
		return nil, errors.New("user not found")
	}

	return user, nil
}

func GetYourSelf(request *rest.ApiRequest) (interface{}, error) {
	if request.GetUser() != nil {
		return request.GetUser(), nil
	}
	return nil, errors.New("user not found")
}

func GetUserRoles(request *rest.ApiRequest) (interface{}, error) {
	db := request.Context.Users.DB
	rows, err := db.Table("user_role").Select("role").Where("user_id = ?", request.GetUser().ID).Rows()
	if err != nil {
		return nil, err
	}

	roles := []string{}

	for rows.Next() {
		role := ""
		err := rows.Scan(&role)
		if err != nil {
			return nil, err
		}
		roles = append(roles, role)
	}

	return roles, nil
}

func CreateUserHandler(request *rest.ApiRequest) (interface{}, error) {
	data, err := ioutil.ReadAll(request.GetHttpRequest().Body)

	if err != nil {
		return nil, errors.New("missing data")
	}

	userMap := map[string]string{}
	json.Unmarshal(data, &userMap)
	errs := []error{}

	user := models.User{
		Name:     userMap["name"],
		Email:    userMap["email"],
		Password: userMap["password"],
	}
	if len(user.Name) < 1 {
		errs = append(errs, errors.New("Invalid name."))
	}

	if !govalidator.IsEmail(user.Email) {
		errs = append(errs, errors.New("Invalid Email ID."))
	}

	if len(user.Password) < 6 {
		errs = append(errs, errors.New("Password length should be more than 6 characters."))
	}

	if len(errs) > 0 {
		return nil, rest.MultipleError{Errs: errs}
	}

	user.Password = saltAndHash([]byte(user.Password))

	err = request.Context.Users.Create(user)
	if err != nil {
		return nil, errors.New("unable to create user")
	}

	return common.Status{Status: common.SUCCESS, Message: "Successfully created user."}, nil
}

func UpdateCredentialsHandler(request *rest.ApiRequest) (interface{}, error) {
	data, err := ioutil.ReadAll(request.GetHttpRequest().Body)
	if err != nil || len(data) == 0 {
		return nil, errors.New("empty body")
	}

	userMap := map[string]string{}
	json.Unmarshal(data, &userMap)

	parsedUser := models.User{
		Name:     userMap["name"],
		Email:    userMap["email"],
		Password: userMap["password"],
	}
	if err != nil {
		return nil, err
	}

	if parsedUser.ID != request.GetUser().ID {
		return nil, errors.New("wrong user")
	}

	if bcrypt.CompareHashAndPassword([]byte(request.GetUser().Password), []byte(parsedUser.Password)) == nil {
		return nil, errors.New("can't use old password again")
	}

	parsedUser.Password = saltAndHash([]byte(parsedUser.Password))
	err = request.Context.Users.UpdatePassword(parsedUser)
	if err != nil {
		return nil, err
	}

	return common.StatusSuccess("updated password"), nil
}

func GetUserAddresses(request *rest.ApiRequest) (interface{}, error) {
	return request.Context.Addresses.GetAddressesBy(request.GetUser())
}

func validateAddress(address models.Address) []error {
	return nil
}

func SaveAddress(request *rest.ApiRequest) (interface{}, error) {
	body, err := ioutil.ReadAll(request.GetHttpRequest().Body)
	if err != nil {
		return nil, errors.New("bad data")
	}

	address := models.Address{}
	err = json.Unmarshal(body, &address)
	if err != nil {
		return nil, errors.New("bad data")
	}

	errs := validateAddress(address)
	if len(errs) > 0 {
		return nil, rest.MultipleError{errs}
	}

	address.UserId = request.GetUser().ID
	err = request.Context.Addresses.Save(request.GetUser(), address)
	if err != nil {
		fmt.Println(err)
		return nil, errors.New("unable to save address")
	}

	return common.Status{common.SUCCESS, "successfully saved address"}, nil
}

func DeleteAddress(request *rest.ApiRequest) (interface{}, error) {
	body, err := ioutil.ReadAll(request.GetHttpRequest().Body)
	if err != nil {
		return nil, errors.New("bad data")
	}

	address := models.Address{}
	err = json.Unmarshal(body, &address)
	if err != nil {
		return nil, errors.New("bad data")
	}

	err = request.Context.Addresses.Delete(request.GetUser(), address)
	if err != nil {
		return nil, errors.New("unable to delete address")
	}
	return common.Status{common.SUCCESS, "successfully saved address"}, nil
}
