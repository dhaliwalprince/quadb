package routes

import (
	"golang.org/x/crypto/bcrypt"
)

func saltAndHash(pwd []byte) string {
	res, err := bcrypt.GenerateFromPassword(pwd, bcrypt.DefaultCost)
	if err != nil {
		panic(err)
	}

	return string(res)
}

var SaltAndHash = saltAndHash
