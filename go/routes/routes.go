package routes

import (
	log2 "log"
	"strings"

	"bitbucket.org/dhaliwalprince/quadb/go/auth"
	"bitbucket.org/dhaliwalprince/quadb/go/common/log"
	context2 "bitbucket.org/dhaliwalprince/quadb/go/context"
	"bitbucket.org/dhaliwalprince/quadb/go/models"
	"bitbucket.org/dhaliwalprince/quadb/go/rest"
	"bitbucket.org/dhaliwalprince/quadb/go/services"
	"github.com/gorilla/mux"
)

var ApiRouter *mux.Router
var Ctx *rest.Context

func InitRoutes(router *mux.Router) {
	s := router
	context := &rest.Context{
		Users:     services.NewUserStore(context2.Config.GormDb),
		Addresses: services.NewAddressStore(context2.Config.GormDb),
		Designs:   services.NewDesignStore(context2.Config.GormDb),
		Orders:    services.NewOrderStore(context2.Config.GormDb),
		Catalog:   services.NewCatalog(context2.Config.GormDb),
		Config:    context2.Config,
	}

	Ctx = context

	ApiRouter = s

	models.InitializeModels(context2.Config.GormDb)

	// user handlers
	s.HandleFunc("/user", rest.MakeGenericApiHandler(auth.UserInterceptor(GetYourSelf), context)).Methods("GET")
	s.HandleFunc("/auth/token", rest.MakeGenericApiHandler(auth.UserInterceptor(auth.GetAuthTokenByUsernameAndPassword), context)).Methods("GET")
	s.HandleFunc("/users", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(GetUsersHandler), context)).Methods("GET")
	s.HandleFunc("/users/{id:[0-9]+}", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(GetUserByIdHandler), context)).Methods("GET")
	s.HandleFunc("/user/roles", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(GetUserRoles), context)).Methods("GET")
	s.HandleFunc("/user", rest.MakeGenericApiHandler(CreateUserHandler, context)).Methods("POST")
	s.HandleFunc("/user", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(UpdateCredentialsHandler), context)).Methods("PUT")

	// addresses handlers
	s.HandleFunc("/user/addresses", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(SaveAddress), context)).Methods("POST")
	s.HandleFunc("/user/addresses", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(GetUserAddresses), context)).Methods("GET")
	s.HandleFunc("/user/addresses", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(DeleteAddress), context)).Methods("DELETE")

	// design handlers
	s.HandleFunc("/user/designs", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(GetDesignsByUser), context)).Methods("GET")
	s.HandleFunc("/user/designs", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(SaveDesign), context)).Methods("POST")
	s.HandleFunc("/user/designs", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(DeleteDesign), context)).Methods("DELETE")
	s.HandleFunc("/user/designs/h/{hash}", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(FindDesignByHashHandler), context)).Methods("GET")

	// orders handler
	s.HandleFunc("/user/cart", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(PlaceOrderInCart), context)).Methods("POST")
	s.HandleFunc("/user/orders/{id:[0-9]+}", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(GetOrderByIdHandler), context)).Methods("GET")
	s.HandleFunc("/user/orders/{id:[0-9]+}/place", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(PlaceOrderHandler), context)).Methods("GET")
	s.HandleFunc("/user/orders/{id:[0-9]+}/cancel", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(CancelOrderHandler), context)).Methods("GET")
	s.HandleFunc("/user/orders/{id:[0-9]+}/info", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(UpdateOrderInfo), context)).Methods("POST")
	s.HandleFunc("/user/orders/{id:[0-9]+}/remove", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(RemoveFromCartHandler), context)).Methods("DELETE")
	s.HandleFunc("/user/orders/request", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(RequestOrderHandler), context)).Methods("POST")
	s.HandleFunc("/user/orders/request/quantity", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(UpdateRequestQuantityHandler), context)).Methods("PUT")
	s.HandleFunc("/user/orders/request/status", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(UpdateRequestStatusHandler), context)).Methods("PUT")
	s.HandleFunc("/user/orders/requests", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(ListUserRequests), context)).Methods("GET")
	s.HandleFunc("/user/orders", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(GetUserOrdersHandler), context)).Methods("GET")
	s.HandleFunc("/user/orders/requests/{requestId:[0-9]+}", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(GetUserRequestHandler), context)).Methods("GET")
	s.HandleFunc("/user/orders/requests/{requestId:[0-9]+}/cancel", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(CancelRequestHandler), context)).Methods("GET")

	// orders handler for staff/admin
	s.HandleFunc("/users/{userId:[0-9]+}/orders/{orderId:[0-9]+}/reject", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(RejectOrderByStaffHandler), context)).Methods("GET")
	s.HandleFunc("/users/{userId:[0-9]+}/orders/{orderId:[0-9]+}/accept", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(AcceptOrderHandler), context)).Methods("GET")
	s.HandleFunc("/users/{userId:[0-9]+}/orders/{orderId:[0-9]+}/mark_delivered", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(MarkOrderDeliveredHandler), context)).Methods("GET")
	s.HandleFunc("/orders", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(ListAllOrders), context)).Methods("GET")
	s.HandleFunc("/orders/{id:[0-9]+}", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(FindOrderHandler), context)).Methods("GET")
	s.HandleFunc("/orders/requests", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(ListAllRequests), context)).Methods("GET")
	s.HandleFunc("/orders/requests/{requestId:[0-9]+}", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(GetRequestByIdHandler), context)).Methods("GET")
	s.HandleFunc("/orders/requests/{requestId:[0-9]+}/setStatus", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(ChangeRequestStatus), context)).Methods("GET")
	s.HandleFunc("/orders/requests/{requestId:[0-9]+}/quotations", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(ListQuotationHandlerForRequest), context)).Methods("GET")
	s.HandleFunc("/orders/requests/{requestId:[0-9]+}/quotations", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(CreateQuotationHandler), context)).Methods("POST")
	s.HandleFunc("/orders/requests/{requestId:[0-9]+}/quotations/{quotationId:[0-9]+}", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(DeleteQuotationHandler), context)).Methods("DELETE")
	s.HandleFunc("/quotations/{quotationId:[0-9]+}", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(GetQuotationByIdHandler), context)).Methods("GET")

	// catalog handlers
	//  categories handler
	s.HandleFunc("/catalog/categories", rest.MakeGenericApiHandler(GetAllCategoriesHandler, context)).Methods("GET")
	s.HandleFunc("/catalog/categories", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(AddCategoryHandler), context)).Methods("POST")
	s.HandleFunc("/catalog/categories", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(UpdateCategoryHandler), context)).Methods("PUT")
	s.HandleFunc("/catalog/categories/{id:[0-9]+}", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(DeleteCategoryHandler), context)).Methods("DELETE")

	//  material handler
	s.HandleFunc("/catalog/materials", rest.MakeGenericApiHandler(GetAllMaterials, context)).Methods("GET")
	s.HandleFunc("/catalog/materials", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(AddMaterialHandler), context)).Methods("POST")
	s.HandleFunc("/catalog/materials", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(UpdateMaterialHandler), context)).Methods("PUT")
	s.HandleFunc("/catalog/materials/{id:[0-9]+}", rest.MakeGenericApiHandler(auth.StaffRequestInterceptor(DeleteMaterialHandler), context)).Methods("DELETE")

	// image upload service
	uploadService := UploadService{AssetsFolder: "public"}

	log.I("Initializing upload service")
	err := uploadService.InitializeService(context2.Config)
	if err != nil {
		log2.Printf("error caught: %v", err)
	}
	s.HandleFunc("/assets/images/public/{image}", rest.MakeGenericApiHandler(uploadService.GetPublicImage, context)).Methods("GET")
	s.HandleFunc("/assets/images/public", rest.MakeGenericApiHandler(uploadService.UploadPublicImage, context)).Methods("POST")
	s.HandleFunc("/assets/images/private/{image}", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(uploadService.GetUserImage), context)).Methods("GET")
	s.HandleFunc("/assets/images/private", rest.MakeGenericApiHandler(auth.AuthenticatedUserOnly(uploadService.UploadUserImage), context)).Methods("POST")
	InitMetaApis()
	fileServer := NewFileServer(context2.Config)
	fileServer.InstallHttpHandlers(s, Ctx)

	router.Walk(func(route *mux.Route, rtr *mux.Router, ancestors []*mux.Route) error {
		p, _ := route.GetPathTemplate()
		m, _ := route.GetMethods()
		log2.Printf("%s with methods [%s]\n", p, strings.Join(m, ", "))
		return nil
	})
}
