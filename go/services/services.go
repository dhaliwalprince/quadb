package services

import (
	"bitbucket.org/dhaliwalprince/quadb/go/store"
	"github.com/jinzhu/gorm"
)

func NewAddressStore(db *gorm.DB) *store.AddressStore {
	return &store.AddressStore{
		DB: db,
	}
}

func NewUserStore(db *gorm.DB) *store.UserStore {
	return &store.UserStore{
		DB: db,
	}
}

// this uses gorm DB
func NewDesignStore(db *gorm.DB) *store.DesignStore {
	return &store.DesignStore{
		DB: db,
	}
}

func NewOrderStore(db *gorm.DB) *store.OrderStore {
	return &store.OrderStore{
		DB: db,
	}
}

func NewCatalog(db *gorm.DB) *store.Catalog {
	return &store.Catalog{
		DB: db,
	}
}
