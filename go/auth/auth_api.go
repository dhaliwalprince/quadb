package auth

import (
	"errors"
	"fmt"
	"net/http"
	"reflect"
	"time"

	"bitbucket.org/dhaliwalprince/quadb/go/common/roles"
	"bitbucket.org/dhaliwalprince/quadb/go/models"
	"bitbucket.org/dhaliwalprince/quadb/go/rest"
	"github.com/dgrijalva/jwt-go"
	request2 "github.com/dgrijalva/jwt-go/request"
	"golang.org/x/crypto/bcrypt"
	"log"
)

// TODO: constants should be moved out of source code
const AUTHOR = "Prince Dhaliwal"
const BASE_KEY = "dhaliwal"

var SIGNING_KEY = []byte(BASE_KEY)

func AuthError(message string) error {
	return errors.New(message)
}

type AuthJwtClaims struct {
	UserId string `json:"user_id"`
	jwt.StandardClaims
}

type JwtResponse struct {
	Token string `json:"token"`
}

func BuildClaims(user *models.User) jwt.MapClaims {
	claims := jwt.MapClaims{
		"Issuer":    AUTHOR,
		"IssuedAt":  time.Now().Unix(),
		"IssuedFor": time.Hour * 24 * 15,
		"usr":       user.ID,
	}

	return claims
}

func GetAuthTokenByUsernameAndPassword(request *rest.ApiRequest) (interface{}, error) {
	if request.GetUser() == nil {
		return nil, AuthError("Invalid/missing credentials.")
	}

	claims := BuildClaims(request.GetUser())
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(SIGNING_KEY)

	if err != nil {
		panic(err)
	}

	cookie := &http.Cookie{}
	cookie.Name = "token"
	cookie.Value = tokenString
	cookie.Path = "/"
	cookie.Expires = time.Now().Add(30 * 24 * time.Hour)

	http.SetCookie(request.Writer, cookie)
	return JwtResponse{Token: tokenString}, nil
}

func JwtStringExtractor(tokenString string, context *rest.Context) (*models.User, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return SIGNING_KEY, nil
	})

	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		fmt.Println(token)
		return nil, fmt.Errorf("claims could not be identified: %v", token.Claims)
	}

	userId, ok := claims["usr"].(float64)
	if !ok {
		return nil, fmt.Errorf("unable to parse claims: %v, %v", claims, reflect.TypeOf(claims["usr"]))
	}

	id := (int)(userId)
	user, err := context.Users.GetUserById(id)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func JwtUserExtractor(request *http.Request, context *rest.Context) (*models.User, error) {
	tokenString, err := request2.OAuth2Extractor.ExtractToken(request)
	if err != nil {
		return nil, err
	}

	return JwtStringExtractor(tokenString, context)
}

func CookieAuthExtractor(request *http.Request, context *rest.Context) (*models.User, error) {
	cookie, err := request.Cookie("token")
	if err != nil {
		return nil, errors.New("unable to parse cookie: " + err.Error())
	}

	return JwtStringExtractor(cookie.Value, context)
}

func BasicAuthUserExtractor(request *http.Request, context *rest.Context) (*models.User, error) {
	email, password, ok := request.BasicAuth()
	if !ok {
		email = request.Form.Get("email")
		password = request.Form.Get("password")
	}
	user, err := context.Users.GetUserByEmail(email)
	if err != nil {
		return nil, err
	}

	if err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)); err != nil {
		return nil, err
	}

	return user, nil
}

// utility function for extracting user from request
func ExtractUser(request *rest.ApiRequest) (*models.User, error) {
	if request.GetUser() != nil {
		return request.GetUser(), nil
	}
	user, err := BasicAuthUserExtractor(request.GetHttpRequest(), request.Context)
	if err == nil {
		return user, nil
	}

	user, err = JwtUserExtractor(request.GetHttpRequest(), request.Context)
	if err == nil {
		return user, nil
	}

	return CookieAuthExtractor(request.GetHttpRequest(), request.Context)
}

// this helper function intercepts request and will read the Authorization header
// and will try to get information about the user
func UserInterceptor(next rest.GenericApiHandler) rest.GenericApiHandler {
	return rest.GenericApiHandler(func(request *rest.ApiRequest) (interface{}, error) {
		user, err := ExtractUser(request)

		if err != nil {
			next(request)
		}

		request.SetUser(user)
		return next(request)
	})
}

func HasRole(context *rest.Context, user *models.User, role roles.Role) bool {
	db := context.Users.DB
	row := db.Table("user_role").Select("user_id").Where("user_id = ? and role = ?", user.ID, string(role))

	u := struct {
		UserId uint
	}{}
	err := row.Scan(&u).Error
	return err == nil && u.UserId == user.ID
}

func RoleBasedRequestInterceptor(next rest.GenericApiHandler, role roles.Role) rest.GenericApiHandler {
	return rest.GenericApiHandler(func(request *rest.ApiRequest) (interface{}, error) {
		user, err := ExtractUser(request)

		if err != nil {
			return nil, AuthError("invalid/missing credentials")
		}

		if HasRole(request.Context, user, role) {
			request.SetUser(user)
			return next(request)
		}

		return nil, AuthError("not enough permissions")
	})
}

// middleware for authenticating request only to staff members
func StaffRequestInterceptor(next rest.GenericApiHandler) rest.GenericApiHandler {
	return RoleBasedRequestInterceptor(next, roles.STAFF)
}

// for admin
func AdminRequestInterceptor(next rest.GenericApiHandler) rest.GenericApiHandler {
	return RoleBasedRequestInterceptor(next, roles.ADMIN)
}

// for authenticated user only
func AuthenticatedUserOnly(next rest.GenericApiHandler) rest.GenericApiHandler {
	return func(request *rest.ApiRequest) (interface{}, error) {
		user, err := ExtractUser(request)
		if err != nil {
			log.Printf("%v", err)
			return nil, errors.New("missing credentials")
		}

		request.SetUser(user)
		return next(request)
	}
}
