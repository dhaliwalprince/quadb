// common data structure for implementing various api calls
package rest

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"io"

	"bitbucket.org/dhaliwalprince/quadb/go/common"
	"bitbucket.org/dhaliwalprince/quadb/go/common/log"
	"bitbucket.org/dhaliwalprince/quadb/go/context"
	"bitbucket.org/dhaliwalprince/quadb/go/models"
	"bitbucket.org/dhaliwalprince/quadb/go/store"
	"github.com/gorilla/mux"
)

type ApiError interface {
	JSON() []byte
}

type NotFound struct {
}

type ServerError struct{}

type UnauthorizedError struct {
}

type MultipleError struct {
	Errs []error `json:"errors"`
}

type ResponseStream struct {
	ContentType string
	Stream      io.Reader
}

func (error ServerError) Error() string {
	return fmt.Sprintf("internal server error")
}

func (error MultipleError) Error() string {
	return fmt.Sprintf("%v", error.Errs)
}

func (error NotFound) Error() string {
	return fmt.Sprintf("resource you are looking for doesn't exist")
}

func (error UnauthorizedError) Error() string {
	return fmt.Sprintf("permission denied to view the resource")
}

type singleError struct {
	common.Status
	Errors []string `json:"errors"`
}

func (error MultipleError) JSON() []byte {
	errors := []string{}
	for _, err := range error.Errs {
		errors = append(errors, err.Error())
	}

	data, _ := json.Marshal(singleError{common.Status{Status: common.FAILED}, errors})
	return data
}

func Success(message string) common.Status {
	return common.Status{common.SUCCESS, message}
}

// helper struct for handling redirects
type Redirect struct {
	Url  string
	Code int
}

// this context is common between all the requests and represents
// various interfaces for accessing databases
type Context struct {
	Users     *store.UserStore
	Addresses *store.AddressStore
	Designs   *store.DesignStore
	Orders    *store.OrderStore
	Catalog   *store.Catalog
	Config    *context.Configuration
}

// this represents api request it contains information about user
// who is requesting and the request itself
type ApiRequest struct {
	Context *Context
	Vars    map[string]string
	request *http.Request
	user    *models.User
	Writer  http.ResponseWriter
}

func (context *Context) BuildApiRequest(request *http.Request) ApiRequest {
	return ApiRequest{request: request, Context: context, Vars: mux.Vars(request)}
}

func (request *ApiRequest) GetUser() *models.User {
	return request.user
}

func (request *ApiRequest) SetUser(user *models.User) {
	request.user = user
}

func (request *ApiRequest) GetHttpRequest() *http.Request {
	return request.request
}

// helper method to parse the request body (only in json) to `to`
func (request *ApiRequest) ParseBody(to interface{}) error {
	data, err := ioutil.ReadAll(request.request.Body)
	if err != nil {
		return err
	}

	return json.Unmarshal(data, to)
}

type GenericApiHandler func(request *ApiRequest) (interface{}, error)

// handles error gracefully
func HandleError(err error, w http.ResponseWriter, r *http.Request) {
	if e, ok := err.(ApiError); ok {
		w.Write(e.JSON())
		return
	} else if n, ok := err.(NotFound); ok {
		w.WriteHeader(http.StatusNotFound)
		w.Write(common.Q(map[string]string{"message": n.Error(), "status": "FAILED"}))
		return
	} else if a, ok := err.(UnauthorizedError); ok {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write(common.Q(map[string]string{"message": a.Error(), "status": "FAILED"}))
		return
	}

	w.Write(common.StatusFailed(err.Error()))
}

// wraps a GenericApiHandler to a http.HandlerFunc
func MakeGenericApiHandler(handler GenericApiHandler, context *Context) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		request := context.BuildApiRequest(r)
		request.Writer = w
		data, err := handler(&request)
		if err != nil {
			HandleError(err, w, r)
			return
		}

		if s, ok := data.(ResponseStream); ok {
			w.Header().Add("Content-Type", s.ContentType)
			w.WriteHeader(http.StatusOK)
			_, err = io.Copy(w, s.Stream)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				log.W("%v", err)
			}
			return
		} else if redirect, ok := data.(Redirect); ok {
			http.Redirect(w, r, redirect.Url, redirect.Code)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(common.Q(data))
	})
}
