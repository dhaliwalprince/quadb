package hooks

import "github.com/gorilla/mux"

func Install(router *mux.Router) {
	router.HandleFunc("/bitbucket/repo", pushHookHandler)
}
