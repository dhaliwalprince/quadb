package hooks

import (
	"net/http"
	"io/ioutil"
	"time"
	"encoding/json"
	"log"
	"bitbucket.org/dhaliwalprince/quadb/go/utils"
)

type User struct {
	Username string `json:"username"`
	Type string `json:"type"`
	DisplayName string `json:"display_name"`
	UUID string `json:"uuid"`
}

type Author struct {
	Raw string `json:"raw"`
	Type string `json:"type"`

}

type Link struct {
	Href string `json:"href"`
}

type Links struct {
	HTML Link `json:"html"`
}

type CommitSummary struct {
	Raw string `json:"raw"`
	Markup string `json:"markup"`
	HTML string `json:"html"`
	Type string `json:"type"`
}

type Commit struct {
	Hash string `json:"hash"`
	Author Author `json:"author"`
	Summary CommitSummary `json:"summary"`
	Date time.Time `json:"date"`
	Message string `json:"message"`
	Type string `json:"type"`
}

type Repository struct {
	SCM string `json:"scm"`
	Name string `json:"name"`
	FullName string `json:"full_name"`
	Owner User `json:"owner"`
}

type BitBucketChange struct {
	Forced bool `json:"forced"`
	Old Commit `json:"old"`
	New Commit `json:"new"`
	Commits []Commit `json:"commits"`
}

type BitBucketPushHookLoad struct {
	Changes []BitBucketChange
}

type BitBucketHookLoad struct {
	Load *BitBucketPushHookLoad `json:"push"`
	Actor User `json:"actor"`
	Repository Repository `json:"repository"`
}

func pushHookHandler(w http.ResponseWriter, r *http.Request) {
	d, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	load := BitBucketHookLoad{}
	err = json.Unmarshal(d, &load)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	hash := load.Load.Changes[0].Commits[0].Hash
	message := load.Load.Changes[0].Commits[0].Summary.Raw
	log.Printf("New version of website being updated: %s (%s)", hash, message)
	utils.SendSlackMessage("Website is being updated to version "+hash+" ("+message+").")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Webhook processed"))
}
