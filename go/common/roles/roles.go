package roles

type Role string

// just two roles to differentiate between admin and user
const ADMIN Role = "ADMIN"
const USER Role = "USER"
const STAFF Role = "STAFF"
