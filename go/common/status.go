package common

import "encoding/json"

const SUCCESS = "SUCCESS"
const FAILED = "FAILED"

type Status struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

type Error struct {
	Error string
}

func StatusFailed(message string) []byte {
	res, _ := json.Marshal(Status{
		Status:  "FAILED",
		Message: message,
	})

	return res
}

func StatusSuccess(message string) []byte {
	res, _ := json.Marshal(Status{
		Status:  "SUCCESS",
		Message: message,
	})

	return res
}

func StatusFailedWithErrors(errs []error) []byte {
	res := make(map[string]interface{})
	errors := []Error{}

	for _, err := range errs {
		e := Error{
			Error: err.Error(),
		}

		errors = append(errors, e)
	}

	res["err_list"] = errors
	res["status"] = "FAILED"

	r, err := json.Marshal(res)

	if err != nil {
		panic(err)
	}
	return r
}
