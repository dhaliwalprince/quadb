package log

import (
	"log"
)

func W(format string, data ...interface{}) {
	log.Printf(format, data...)
}

func I(format string, data ...interface{}) {
	log.Printf(format, data...)
}

func E(format string, data ...interface{}) {
	log.Panicf(format, data...)
}
