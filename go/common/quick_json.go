package common

import "encoding/json"

func Q(o interface{}) []byte {
	res, err := json.Marshal(o)

	if err != nil {
		panic(err)
	}

	return res
}
