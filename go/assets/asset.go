package assets

import (
	"os"
	"path"
)

const AssetRootDir = "cdnAssets"

func Open(name string) (*os.File, error) {
	return os.Open(path.Join(AssetRootDir, name))
}
