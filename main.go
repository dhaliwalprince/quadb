package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"

	"bitbucket.org/dhaliwalprince/quadb/go/blog"
	"bitbucket.org/dhaliwalprince/quadb/go/cdn"
	"bitbucket.org/dhaliwalprince/quadb/go/context"
	"bitbucket.org/dhaliwalprince/quadb/go/models"
	"bitbucket.org/dhaliwalprince/quadb/go/routes"
	"bitbucket.org/dhaliwalprince/quadb/go/tmpl"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"bitbucket.org/dhaliwalprince/quadb/go/hooks"
)

func installFileServer(p string, router *http.ServeMux) {
	router.Handle(p[len("build"):], http.StripPrefix(p[len("build"):], http.FileServer(http.Dir(p))))
}

func installBuildFolder(dir string, router *http.ServeMux) {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		log.Printf("Unable to install file server on %s\n", dir)
		return
	}

	for _, file := range files {
		p := path.Join(dir, file.Name())
		if file.IsDir() {
			log.Printf("%s %s mounted on %s", "GET", p[len("build"):], p)
			installFileServer(p, router)
			installBuildFolder(p, router)
		}
	}
}

func main() {
	log.SetOutput(os.Stdout)
	log.SetFlags(log.Llongfile)
	log.SetPrefix("quadb.in: ")
	defer context.Config.GormDb.Close()

	context.Config.GormDb.SingularTable(true)
	router := mux.NewRouter()

	t := tmpl.GetTemplateContext()
	blogRouter := mux.NewRouter()
	blog.Install(t, context.Config, blogRouter)

	routes.SetTemplateContext(t)
	// Initialize REST endpoints
	routes.InitRoutes(router)

	// Initialize build folder
	//router.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(http.Dir("build"))))

	db := context.Config.GormDb
	admin := models.User{Name: "Admin", Email: "admin@quadb.in", Password: routes.SaltAndHash([]byte("admin_password"))}

	u := models.User{}
	err := db.Where("email = ?", admin.Email).First(&u).Error
	if err != nil {
		log.Println("First run, installing default users...")
		db.Save(&admin)
		db.Save(&models.UserRole{UserId: admin.ID, Role: "ADMIN"})
		db.Save(&models.UserRole{UserId: admin.ID, Role: "STAFF"})
	}
	m := http.NewServeMux()
	installBuildFolder("build", m)

	cdnServer := cdn.NewServer()
	m.Handle("/api/", http.StripPrefix("/api", router))

	hookRouter := mux.NewRouter()
	hooks.Install(hookRouter)
	m.Handle("/hooks/", http.StripPrefix("/hooks", hookRouter))
	m.Handle("/company/", http.StripPrefix("/company", blogRouter))
	//m.Handle("/about-us", http.RedirectHandler("/company/blog/about-us", http.StatusTemporaryRedirect))
	m.Handle("/cdn/", http.StripPrefix("/cdn", cdnServer))

	m.HandleFunc("/admin/", routes.AdminRootHandler)

	// these are for designer
	m.Handle("/lang/", http.FileServer(http.Dir("cdnAssets/mainapp")))
	m.Handle("/static/", http.FileServer(http.Dir("cdnAssets/mainapp")))
	m.Handle("/js/", http.FileServer(http.Dir("cdnAssets/mainapp")))
	m.Handle("/css/", http.FileServer(http.Dir("cdnAssets/mainapp")))
	m.Handle("/fonts/", http.FileServer(http.Dir("cdnAssets/mainapp")))
	m.Handle("/html/", http.FileServer(http.Dir("cdnAssets/mainapp")))
	m.Handle("/icons/", http.FileServer(http.Dir("cdnAssets/mainapp")))
	m.Handle("/images/", http.FileServer(http.Dir("cdnAssets/mainapp")))
	m.Handle("/blog", http.RedirectHandler("/company/blogs", http.StatusSeeOther))
	m.Handle("/about-us", http.RedirectHandler("/maintenance", http.StatusSeeOther))
	m.Handle("/print-varieties", http.RedirectHandler("/maintenance", http.StatusSeeOther))
	m.Handle("/clients", http.RedirectHandler("/maintenance", http.StatusSeeOther))
	m.Handle("/payment", http.RedirectHandler("/maintenance", http.StatusSeeOther))
	m.Handle("/launch-designer", http.RedirectHandler("/designer", http.StatusSeeOther))

	m.HandleFunc("/", routes.RootHandler)
	log.Printf("Starting server at %s", ":8080")
	log.Printf("%s", http.ListenAndServe(":8080", handlers.LoggingHandler(os.Stdout, m)))
}
